<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

$domain = $_SERVER['SERVER_NAME'];

if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
  include( dirname( __FILE__ ) . '/wp-config-local.php' );
  
// Otherwise use the below settings (on live server)
} else {
 
  // Live Server Database Settings
  define( 'DB_NAME',     'aacpmemdev');
  define( 'DB_USER',     'aacpmemdev');
  define( 'DB_PASSWORD', 'aacpmemdev123' );
  define( 'DB_HOST',     'localhost'  );
  
  // Overwrites the database to save keep edeting the DB
  define('WP_HOME','http://www.aacp.org.uk');
  define('WP_SITEURL','http://www.aacp.org.uk');
  
  // Turn Debug off on live server
  define('WP_DEBUG', false);
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>BQbL9xfM,4]xt2~_Pq!sY_63LcV>]!-mqs7pAKx@n^SWC%iKyJ`-#_},R<+ =}j');
define('SECURE_AUTH_KEY',  'qGCk4W)di6mKqRVlIZO<,k9hz<]HlS;!kCE?P-]/ jhL[.y7:o9i%53R{1bi/!em');
define('LOGGED_IN_KEY',    'YRpG]0LzS/RJw#!+lAd}u]`@~kw1+1V[=y~f/&(Lg:6}ObIyPK#utmgy+O+_}?n$');
define('NONCE_KEY',        '7=5U{xv1CfWb,VKZ]S}$akjHUkK1ASD=:%9DvN&0t~K(`7{x9t?u6`oz!%a!iTs#');
define('AUTH_SALT',        'X?1;m)vws4Ly*T6;B8N9pwHE>Q#{QC2*R#W5a%RoiK~RFa&perQT,K|8h#5K <3E');
define('SECURE_AUTH_SALT', 'hM:]hkb:u~wDT[5sJ])+?<r{@m4*6;35l?oMk.InO+u>3d42[n2s.(88n8W)?+*4');
define('LOGGED_IN_SALT',   'jk]^G]Qvp@SJn:jZH}Z&pNRKtLQHD;v%=VJ=V62$/-ub5z-@__K).v:h&7n{Vc{|');
define('NONCE_SALT',       ')Q/>X[|D?o]tjF7$_cm=I1@*7coV m+fl#*=2G$VH@8wP{cB*OnAlIQG__;P4c&v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

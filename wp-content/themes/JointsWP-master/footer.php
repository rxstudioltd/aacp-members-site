					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row collapse">
							<div class="row collapse">
								<div class="large-12 columns">
									<p class="source-org copyright">&copy; Copyright <?php echo date('Y'); ?> AACP | Registered office: AACP Limited, Sefton House, Adam Court, Newark Road, Peterborough, PE1 5PP (01733) 390007</p>
								</div>
								<div class="large-9 medium-10 columns navColumn">
									<nav role="navigation">
			    						<?php joints_footer_links(); ?>
			    					</nav>
			    				</div>
		    				</div>

						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->
		<!-- all js scripts are loaded in library/joints.php -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->
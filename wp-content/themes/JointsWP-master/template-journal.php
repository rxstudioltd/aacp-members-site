<?php
/*
Template Name: Journals
*/
?>

<?php get_header(); ?>
			
			<div id="content">

				<div id="inner-content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="row">
						<div class="large-12 columns">
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>

					<?php endwhile; endif; ?>

					<div style="display:none;">
						<?php echo do_shortcode('[su_accordion class=""][su_spoiler]Content[/su_spoiler][/su_accordion]'); ?>
					</div>

					<div class="row journalArchive">
						<div class="large-12 columns">
							<hr>
							<h3>Current Issue</h3>
						</div>
						<div class="large-12 columns">
							<?php
								$args = array(
									'post_type' =>'documents',
									'document-type' => 'current-issue',
									'post_per_page' => 1,
								);
								$query = new WP_Query($args);
								while ($query->have_posts()) : $query->the_post();
							?>
								<?php if( get_field('document_upload') ):?>
									<?php 
									$documentuploaded = get_field('document_upload');
									$documenttitle = get_the_title(); 
									echo do_shortcode( '[su_members message="This content is for registered users only. Please %login%." color="#ffcc00" login_text="login" login_url="/wp-login.php" class=""]
										<div class="large-3 column journal-archive-item">
											<img src="/wp-content/uploads/2013/10/icon-document.png">
											<h6>'.$documenttitle.'</h6>
											<br />
											<a class="blue-button" href="'.$documentuploaded.'">DOWNLOAD</a>
										</div>
									[/su_members]' ); 
									?>
				            		
								<?php endif; ?>
							<?php endwhile; ?>
						</div>



						<div class="large-12 columns journalArchiveTitles">
							<hr>
							<h3>Recent Back Issues</h3>
						</div>
						<div class="large-12 columns">
							<?php
								$args = array(
									'post_type' =>'documents',
									'document-type' => 'recent-back-issues',
									'post_per_page' => 1,
								);
								$query = new WP_Query($args);
								while ($query->have_posts()) : $query->the_post();
								
								if( get_field('document_upload') ):
									$documentuploaded = get_field('document_upload');
									$documenttitle = get_the_title(); 
									echo do_shortcode( '[su_members message="This content is for registered users only. Please %login%." color="#ffcc00" login_text="login" login_url="/wp-login.php" class=""]
										<div class="large-3 columns journal-archive-item">
											<img src="/wp-content/uploads/2013/10/icon-document.png">
											<h6>'.$documenttitle.'</h6>
											<br />
											<a class="blue-button" href="'.$documentuploaded.'">DOWNLOAD</a>
										</div>
									[/su_members]' );
									?>
				            		
								<?php endif; ?>
							<?php endwhile; ?>
						</div>



						<div class="large-12 columns journalArchiveTitles">
							<hr>
							<h3>Public Journal Archive</h3>
						</div>
						<div class="large-12 columns">
							<?php
								$args = array(
									'post_type' =>'documents',
									'document-type' => 'archives',
									'orderby' => 'title',
									'order' => 'DESC',
									'post_per_page' => -1,
								);
								$query = new WP_Query($args);
								while ($query->have_posts()) : $query->the_post();
							?>
								<?php if( get_field('document_upload') ):?>
				            		<div class="large-3 columns journal-archive-item">
										<img src="/wp-content/uploads/2013/10/icon-document.png">
										<h6><?php the_title(); ?></h6>
										<br />
										<a class="blue-button" href="<?php the_field('document_upload'); ?>">DOWNLOAD</a>
									</div>
								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					</div>

				</div>						

			</div> <!-- end #content -->

<?php get_footer(); ?>
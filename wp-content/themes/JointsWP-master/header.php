<!doctype html>

  <html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
		<meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

		<!-- Drop Google Analytics here -->
		<!-- end analytics -->
	</head>

	<body <?php body_class(); ?>>
		<div class="off-canvas-wrap" data-offcanvas>
			<div class="inner-wrap">
				<div id="container">
					<header class="header" role="banner">
								 
						<div id="inner-header" class="row">
							<div class="large-6 medium-6 columns">
								<h1>
									<a href="<?php echo home_url(); ?>" rel="nofollow" class="logo">
										<?php bloginfo('name'); ?>
									</a>
								</h1>
							</div>

							
							<div class="large-6 medium-6 columns">
								<div class="loginboxholder">
									<img src="<?php echo get_template_directory_uri(); ?>/library/images/padlock.png"> <span class="loginBoxTitle">MEMBERS LOGIN</span>
									<?php wp_login_form(); ?>
								</div>
							</div>
								  
						</div> <!-- end #inner-header -->
						
					</header> <!-- end .header -->

					<div class="menu-holder">
						<div class="row">
							<?php get_template_part( 'partials/nav', 'main-topbar' ); ?>
						</div>
					</div>




					<?php if ( is_user_logged_in() ) { ?>

					<div class="search-area-holder">
						<div class="row">
							<div class="large-12 columns">
								<?php dynamic_sidebar('Search Area'); ?>
							</div>
						</div>
					</div>

					<div class="members-menu-holder">
						<div class="row">
							<div class="large-12 columns">
								<div class="contain-to-grid">

									<nav class="top-bar" data-topbar>
										<section class="top-bar-section">
											<?php joints_members_nav(); ?>
										</section>
									</nav>
								</div>
							</div>
						</div>
					</div>

					<?php } ?>


<?php
/*
Template Name: Documents
*/
?>

<?php get_header(); ?>
			
			<div id="content">

				<div id="inner-content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="row">
						<div class="large-12 columns">
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>

					<?php endwhile; endif; ?>

					<div style="display:none;">
						<?php echo do_shortcode('[su_accordion class=""][su_spoiler]Content[/su_spoiler][/su_accordion]'); ?>
					</div>

					<div class="row">
						<div class="large-12 columns">
							<?php
								$terms = get_terms('document-type', array( 'parent' => 0 ));
								if ( !empty( $terms ) && !is_wp_error( $terms ) ){
									echo "<div class='su-accordion'>";
									foreach ( $terms as $term ) {
									echo "<div class='su-spoiler su-spoiler-style-fancy su-spoiler-icon-plus su-spoiler-closed'><div class='su-spoiler-title'><span class='su-spoiler-icon'></span>" . $term->name . "</div><div class='su-spoiler-content su-clearfix'>";
									$parent_cat = $term->term_id;
									$taxonomy = 'document-type';
									$cat_children = get_term_children( $parent_cat, $taxonomy );

									if ($cat_children) {
									    foreach($cat_children as $category) {
									    $termed = get_term_by( 'id', $category, $taxonomy );
									    $args=array(
									      'document-type' =>  $termed->name,
									      'post_type' => 'documents',
									      'post_status' => 'publish',
									      'posts_per_page' => -1,
									      'ignore_sticky_posts'=> 1
									    );
									    $my_query = null;
									    $my_query = new WP_Query($args);
									    if( $my_query->have_posts() ) {
									      echo '<div class="documents-inner"><h3>'.$termed->name.'</h3><hr>';
									      while ($my_query->have_posts()) : $my_query->the_post(); ?>
									        <div class="documents-item">
									        	<div class="documents-item-text">
									        		<h5><?php the_title(); ?></h5>
									        		<?php the_content(); ?>
									        	</div>
									        	<div class="documents-item-meta">
									        		<a class="blue-button" href="<?php the_field('document_upload'); ?>">DOWNLOAD</a>
									        	</div>
									        </div>
									        <?php endwhile;
									        echo '</div>';
									    }
									  }
									}
									wp_reset_query();  // Restore global post data stomped by the_post().
									echo "</div></div>";
								}
									echo "</div>";
								}
							?>
						</div>
					</div>

				</div>
			
			</div> <!-- end #content -->

<?php get_footer(); ?>
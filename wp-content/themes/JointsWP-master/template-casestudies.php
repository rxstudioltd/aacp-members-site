<?php
/*
Template Name: Case studies
*/
?>

<?php get_header(); ?>
			
			<div id="content">

				<div id="inner-content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="row">
						<div class="large-12 columns">
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
						</div>
					</div>

					<?php endwhile; endif; ?>

					<div class="row">

						<?php
							$args = array(
								'post_type' =>'Case Studies',
								'post_per_page' => -1,
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
						?>
							<div class="large-12 columns case-study-holder"> 
								<div class="case-study-text">
									<h2><?php the_title(); ?></h2>
									<?php the_content(); ?>
								</div>
								<div class="case-study-info">
									<?php if( get_field('date_created') ):?>
										<div class="info-item">Date created:<br><?php the_field('date_created'); ?></div>
									<?php endif; ?>
									<?php if( get_field('file_size') ):?>
										<div class="info-item">File size:<br><?php the_field('file_size'); ?></div>
									<?php endif; ?>
									<?php if( get_field('file_type') ):?>
										<div class="info-item">File type:<br><?php the_field('file_type'); ?></div>
									<?php endif; ?>
									<?php if( get_field('owner') ):?>
										<div class="info-item">Owner:<br><?php the_field('owner'); ?></div>
									<?php endif; ?>
									<?php if( get_field('attach_document') ):?>
										<div class="info-item-last"><a class="blue-button" href="<?php the_field('attach_document'); ?>">DOWNLOAD</a></div>
									<?php endif; ?>
								</div>
							</div>
						<?php endwhile; ?>

					</div>

				</div>
			
			</div> <!-- end #content -->

<?php get_footer(); ?>
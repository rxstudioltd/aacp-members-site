<?php
/*
Template Name: Members attraction page
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content">
			
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<div class="row">
							<div class="large-12 columns" style="margin-bottom:10px;">
								<?php the_field('who_we_are_title'); ?>
							</div>
							<div class="large-7 columns">
								<img src="<?php the_field('who_we_are_image'); ?>">
							</div>
							<div class="large-5 columns becomepage">
								<?php the_field('who_we_are_text'); ?>
							</div>

							<div class="large-12 columns attractionbreaks">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="large-12 columns" style="margin-bottom:10px;">
								<?php the_field('why_should_title'); ?>
								<br>
							</div>
							<div class="large-5 columns becomepage">
								<?php the_field('why_should_text'); ?>
								<ul class="accordion" data-accordion role="tablist">
									<?php if( have_rows('why_should_accordion') ): ?>

										<?php $counter = 0; ?>		 
									   <?php while( have_rows('why_should_accordion') ): the_row(); ?>

									    	<li class="accordion-navigation">
										   	<a href="#whypanel<?php echo $counter; ?>" role="tab" id="whypanel<?php echo $counter; ?>-heading" aria-controls="whypanel<?php echo $counter; ?>"><?php the_sub_field('why_accordion_title'); ?></a>
										   	<div id="whypanel<?php echo $counter; ?>" class="content" role="tabpanel" aria-labelledby="whypanel<?php echo $counter; ?>-heading">
										   		<?php the_sub_field('why_accordion_text'); ?>
										   	</div>
										   </li>

										   <?php $counter ++; ?>
									        
									<?php endwhile; endif; ?>
								</ul>
							</div>
							<div class="large-7 columns">
								<?php the_field('why_should_video'); ?>
							</div>

							<div class="large-12 columns attractionbreaks">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="large-12 columns" style="margin-bottom:10px;">
								<?php the_field('enrol_title'); ?>
							</div>
							<div class="large-7 columns">
								<img src="<?php the_field('enrol_image'); ?>">
							</div>
							<div class="large-5 columns becomepage">
								<ul class="accordion" data-accordion role="tablist">
									<?php if( have_rows('enrol_accordion') ): ?>
		 								
		 								<?php $counter = 0; ?>
									   <?php while( have_rows('enrol_accordion') ): the_row(); ?>

									    	<li class="accordion-navigation">
										   	<a href="#enrolpanel<?php echo $counter; ?>" role="tab" id="enrolpanel<?php echo $counter; ?>-heading" aria-controls="enrolpanel<?php echo $counter; ?>"><?php the_sub_field('enrol_accordion_title'); ?></a>
										   	<div id="enrolpanel<?php echo $counter; ?>" class="content" role="tabpanel" aria-labelledby="enrolpanel<?php echo $counter; ?>-heading">
										   		<?php the_sub_field('enrol_accordion_text'); ?>
										   	</div>
										  </li>

										  <?php $counter ++; ?>
									        
									<?php endwhile; endif; ?>
								</ul>
							</div>

							<div class="large-12 columns attractionbreaks">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="large-12 columns" style="margin-bottom:10px;">
								<?php the_field('become_title'); ?>
							</div>
							<div class="large-7 columns">
								<img src="<?php the_field('become_image'); ?>">
							</div>
							<div class="large-5 columns becomepage">
								<ul class="accordion" data-accordion role="tablist">
									<?php if( have_rows('become_accordion') ): ?>
		 								
		 								<?php $counter = 0; ?>
									   <?php while( have_rows('become_accordion') ): the_row(); ?>

									    	<li class="accordion-navigation">
										   	<a href="#becomepanel<?php echo $counter; ?>" role="tab" id="becomepanel<?php echo $counter; ?>-heading" aria-controls="becomepanel<?php echo $counter; ?>"><?php the_sub_field('become_accordion_title'); ?></a>
										   	<div id="becomepanel<?php echo $counter; ?>" class="content" role="tabpanel" aria-labelledby="becomepanel<?php echo $counter; ?>-heading">
										   		<?php the_sub_field('become_accordion_text'); ?>
										   	</div>
										  </li>

										  <?php $counter ++; ?>
									        
									<?php endwhile; endif; ?>
								</ul>
							</div>
						</div>

					<?php endwhile; endif; ?>
					    					
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>

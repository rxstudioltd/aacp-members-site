<?php
/*
Template Name: Public news page
*/
?>

<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content">
			
				    <div class="row latestNews">
						<div class="large-12 columns">
							<h3>News</h3>
						</div>
						<?php
							$args = array(
								'post_type' =>'post',
								'category' => 'public-news',
								'order' => 'ASC'
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
						?>
							<div class="large-3 columns newsItem">
								<a href="<?php the_permalink(); ?>"><div class="newsImage"><?php the_post_thumbnail(); ?></div></a>
								<a href="<?php the_permalink(); ?>"><div class="newsTitle">
									<?php if (strlen($post->post_title) > 30) {
									echo substr(the_title($before = '', $after = '', FALSE), 0, 30) . '...'; } else {
									the_title();
									} ?>
								</div></a>
								<div class="newsText"><?php the_excerpt(); ?></div>
							</div>
						<?php endwhile; ?>
						<div class="large-12 columns">
							<?php joints_page_navi(); ?>
						</div>
					</div>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>

<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
			
			<div id="content">


					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="mainBanner">
						<div class="row">
							<div class="large-12 columns">
								<div id="featured" data-orbit data-options="animation: 'fade';
	                                 timer_speed: 4000;
	                                 slide_number: false;
	                                 animation_speed: 700;
	                                 pause_on_hover: false;">
									<?php if( have_rows('banners_homepage') ): ?>
		 
									    <?php while( have_rows('banners_homepage') ): the_row(); ?>

										    <a href="<?php the_sub_field('banner_link_homepage'); ?>"><div class="">
										    	<div class="captionholder">
											    	<div class="caption"><?php the_sub_field('banner_caption_homepage'); ?></div>
											    </div>
									    		<img src="<?php the_sub_field('banner_image_homepage'); ?>" alt="AACP Banner image">
									    	</div></a>
									        
									<?php endwhile; endif; ?>
				                </div>
				            </div>
						</div>
					</div>

					<div class="row introText">
						<div class="large-8 columns">
							<?php the_field('introduction_text'); ?>
						</div>
						<div class="large-4 columns mainPartner">
							<img src="<?php the_field('main_partner_image'); ?>" alt="AACP Main Partner image">
						</div>
					</div>

					<div class="fourBlocks">
						<div class="row">
							<?php if( have_rows('four_blue_blocks') ): ?>
		 						
		 						<?php $counter = 0; ?>
							    <?php while( have_rows('four_blue_blocks') ): the_row(); ?>

							    	<a href="<?php the_sub_field('block_link'); ?>"><div class="large-3 small-6 columns block<?php echo $counter; ?>">
							    		<div class="blockIcon"><img src="<?php the_sub_field('block_icon'); ?>"></div>
							    		<div class="blockTitle"><?php the_sub_field('block_title'); ?></div>
							    		<div class="blockText"><?php the_sub_field('block_text'); ?></div>
									</div></a>

								<?php $counter ++; ?>
							        
							<?php endwhile; endif; ?>
						</div>
					</div>

					<div class="publicInfoScroller">
						<div class="row collapse">
							<div class="large-10 columns">
								<div id="featured" data-orbit>

									<?php if( have_rows('public_questions') ): ?>
		 								
		 								<?php $counter = 0; ?>
									    <?php while( have_rows('public_questions') ): the_row(); ?>

										    <div class="" data-orbit-slide="headline-<?php echo $counter; ?>">
										    	<h3><?php the_sub_field('question_title'); ?></h3>
										    	<?php the_sub_field('question_content'); ?>
									    	</div>

									    <?php $counter ++; ?>
									        
									<?php endwhile; endif; ?>

				                </div>
				            </div>
				            <div class="large-2 columns questionButtonHolder">
				            	<div class="">
				            		<?php if( have_rows('public_questions') ): ?>
		 								
		 								<?php $counter = 0; ?>
									    <?php while( have_rows('public_questions') ): the_row(); ?>

									    	<a data-orbit-link="headline-<?php echo $counter; ?>" class="small button questionButton">
										    	<?php the_sub_field('question_title'); ?>
									    	</a>

									    <?php $counter ++; ?>
									        
									<?php endwhile; endif; ?>
				                </div>
				            </div>
						</div>
					</div>

					<?php endwhile; endif; ?>

					<div class="row latestNews">
						<div class="large-12 columns">
							<h3>Latest News</h3>
						</div>
						<?php
							$args = array(
								'post_type' =>'post',
								'posts_per_page' => 4,
								'order' => 'ASC'
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
						?>
							<div class="large-3 columns">
								<a href="<?php the_permalink(); ?>"><div class="newsImage"><?php the_post_thumbnail(); ?></div></a>
								<a href="<?php the_permalink(); ?>"><div class="newsTitle">
									<?php if (strlen($post->post_title) > 30) {
									echo substr(the_title($before = '', $after = '', FALSE), 0, 30) . '...'; } else {
									the_title();
									} ?>
								</div></a>
								<div class="newsText"><?php the_excerpt(); ?></div>
							</div>
						<?php endwhile; ?>
					</div>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="membersAttraction">
						<div class="row">
							<div class="large-8 medium-12 small-12 columns">
								<div class="promoVideo">
									<?php the_field('attraction_video'); ?>
								</div>
							</div>
							<div class="large-4 medium-12 small-12 columns attractionInfo">
								<h3 class="attractionTitle"><?php the_field('attraction_title'); ?></h3>
								<div class="attractionSubText"><?php the_field('attraction_sub-text'); ?></div>
								<div class="attractionIcons"><img src="<?php the_field('attraction_icons'); ?>"></div>
								<a class="attractionButton" href="<?php the_field('attraction_link'); ?>">Show me how</a>
							</div>
						</div>
					</div>

					<?php endwhile; endif; ?>


			</div> <!-- end #content -->

<?php get_footer(); ?>
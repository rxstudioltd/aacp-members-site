<?php get_header(); ?>
			

			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
					
						<h1 class="archive-title"><span><?php _e('Search Results for:', 'jointstheme'); ?></span> <?php echo esc_attr(get_search_query()); ?></h1>

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
								<h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<?php the_excerpt('<button class="tiny">Read more...</button>'); ?> 
								<hr>
					
						<?php endwhile; ?>	
					
						        <?php joints_page_navi(); ?>	
					
					    <?php else : ?>
					
					    		<h1>Sorry, No Results.</h1>
					    		<p>Try your search again.</p>
                                <p><?php get_search_form(); ?></p>
					
					    <?php endif; ?>
					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->
			
<?php get_footer(); ?>

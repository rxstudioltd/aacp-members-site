<?php
/*
Template Name: Members Homepage
*/
?>

<?php get_header(); ?>
			
			<div id="content">

				<div id="inner-content">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


					<div class="row introText">
						<div class="large-12 columns">
							<?php global $current_user;
      							get_currentuserinfo();
	      					?>
							<h1>Welcome <?php echo '' . $current_user->user_firstname . ' '; echo '' . $current_user->user_lastname . ''; ?></h1>
							<?php the_content(); ?>
						</div>
					</div>


					<?php endwhile; endif; ?>

					<p>&nbsp;</p>


					<div class="row">

						<?php
							$args = array(
								'post_type' =>'documents',
								'document-type' => 'current-issue',
								'posts_per_page' => 1
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
							$documentuploaded = get_field('document_upload');
						?>
						<div class="large-3 columns">
							<div class="hover-item">
								<div class="view view-first">
									<?php echo '<a href="'.$documentuploaded.'">'; ?><img src="<?php echo get_template_directory_uri('template_directory'); ?>/library/images/members-home-journal.png" alt="" /></a>
								</div>
								<div class="lp-item-caption-container">
									<h5>Latest Journal</h5>
									<div class="lp-item-container-border">
									</div>
								</div>
							</div>
							<div class="lp-item-text-container">Download the latest edition of the Journal</div>
						</div>
						<?php endwhile; ?>



						<?php
							$args = array(
								'post_type' =>'page',
								'post__in' => array( 550 )
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
						?>
						<div class="large-3 columns">
							<div class="hover-item">
								<div class="view view-first">
									<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/library/images/members-home-research.png" alt="" /></a>
								</div>
								<div class="lp-item-caption-container">
									<h5><?php the_title(); ?></h5>
									<div class="lp-item-container-border clearfix">
									</div>
								</div>
							</div>
							<div class="lp-item-text-container">Take a look at the latest research from the AACP</div>
						</div>
						<?php endwhile; ?>



						<?php
							$args = array(
								'post_type' =>'page',
								'post__in' => array( 724 )
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
						?>
						<div class="large-3 columns">
							<div class="hover-item">
								<div class="view view-first">
									<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/library/images/members-home-offers.png" alt="" /></a>
								</div>
								<div class="lp-item-caption-container">
									<h5><?php the_title(); ?></h5>
									<div class="lp-item-container-border clearfix">
									</div>
								</div>
							</div>
							<div class="lp-item-text-container">Members only Special Offers from the AACP</div>
						</div>
						<?php endwhile; ?>



						<div class="large-3 columns">
							<div class="hover-item">
								<div class="view view-first">
									<a href="http://www.jcm.co.uk/a-manual-of-acupuncture/ios/"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/library/images/members-home-deadman.png" alt="" /></a>
								</div>
								<div class="lp-item-caption-container">
									<h5>Deadman app</h5>
									<div class="lp-item-container-border clearfix">
									</div>
								</div>
							</div>
							<div class="lp-item-text-container">Visit the Manual of Acupuncture and download the app</div>
						</div>

					</div>

					<p>&nbsp;</p>

					<div class="row latestNews">
						<div class="large-12 columns">
							<h3>Latest News</h3>
						</div>
						<?php
							$args = array(
								'post_type' =>'post',
								'posts_per_page' => 8,
								'order' => 'ASC'
							);
							$query = new WP_Query($args);
							while ($query->have_posts()) : $query->the_post();
						?>
							<div class="large-3 columns">
								<div class="newsImage"><?php the_post_thumbnail(); ?></div>
								<div class="newsTitle">
									<?php if (strlen($post->post_title) > 30) {
									echo substr(the_title($before = '', $after = '', FALSE), 0, 30) . '...'; } else {
									the_title();
									} ?>
								</div>
								<div class="newsText"><?php the_excerpt(); ?></div>
							</div>
						<?php endwhile; ?>
					</div>

				</div>

			</div> <!-- end #content -->

<?php get_footer(); ?>
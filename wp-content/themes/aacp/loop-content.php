<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class(); ?>>
<?php get_template_part( 'includes/single', get_post_format() ); ?>	
				
</div>
 
<?php endwhile; ?>
<?php endif; ?>
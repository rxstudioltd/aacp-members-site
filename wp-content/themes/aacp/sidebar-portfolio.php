<?php
	$project_overview = get_post_meta($post->ID, 'iweb_project_overview', true);
	$project_url = get_post_meta($post->ID, 'iweb_project_link', true);
	$project_btntxt = get_post_meta($post->ID, 'iweb_project_btn', true);
?>

<div class="five columns">			
		<!-- WIDGET -->
			<div class="sidebar-item  m-bot-35">
			
				<div class="caption-container-main m-bot-30">
					<div class="caption-text-container"><?php echo __('PROJECT OVERVIEW','iwebtheme'); ?></div>
					<div class="content-container-white caption-bg"></div>
				</div>

				<div class="content-container-white">
					<p><?php echo $project_overview; ?></p>
				</div>
			</div>	
		<!-- WIDGET -->	
			<div class="sidebar-item  m-bot-35">
				<div class="caption-container-main m-bot-30">
					<div class="caption-text-container"><?php echo __('PROJECT DETAILS','iwebtheme'); ?></div>
					<div class="content-container-white caption-bg clearfix"></div>
				</div>

				<div class="content-container-white">
				<ul class="portfolio-check-list check-icon">
				<?php
					$terms = get_the_terms( $post->ID, 'portfolio_categories' );
					foreach ( $terms as $term ) {
						$cats[0] = $term->name;
						$catlinks[0] = $term->slug;
						$catname = join($catlinks);		
						echo '<li>'.$catname.'</li>';
					}
				?>		
						
					</ul>
				</div>

			</div>

		<!-- WIDGET -->	
			<div class="sidebar-item  m-bot-25">
				<div>
					<a class="button large" href="<?php echo $project_url; ?>" target="_blank">
					<?php echo $project_btntxt; ?></a>
				</div>
			</div>

			<?php dynamic_sidebar('Portfolio Detail Sidebar'); ?>
		
</div>

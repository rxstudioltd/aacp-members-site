<?php
/**
 * @package WordPress
 */
?>
<div class="container clearfix">
	<div class="four columns carousel-intro m-bot-33">
		<div class="caption-container m-bot-20">
			<div class="title-block-text">Latest<br />News</div>
			<div class="carousel-navi jcarousel-scroll">
				<div class="jcarousel-prev"></div>
				<div class="jcarousel-next"></div>
			</div>
		</div>
	</div>

	<div class="jcarousel latest-posts-jc m-bot-50">
		<ul class="clearfix">
			<?php
				$args = array(
					'post_type' =>'post',
					'posts_per_page' => -1,
					'order' => 'ASC'
				);
				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();

				$day=get_the_time('d M Y');
				$title=get_the_title();
				$title = wp_trim_words($title,$num_words =3);
				$title=explode(' ',$title);
				$title[0]='<span class="bold">'.$title[0].'</span>';
				$title=implode(' ',$title);
				$permalink=get_permalink();
				$trimcontent = get_the_content();
				$shortexcerpt = wp_trim_words($trimcontent,$num_words = 18);	
				$image = '';		
				$feathumb = '';
			?>

			<li class="four columns">
				
				<?php if(has_post_thumbnail())  { 
					$featimg = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
					$feathumb = $featimg[0];
					$image = aq_resize($feathumb,400,200, true);
				} ?>
				<div class="hover-item">
					<div class="view view-first">
						<?php if($image != '')  {
							echo '<img src="'.$image.'" alt="" />';	
						} ?>
					</div>
					<div class="lp-item-caption-container">
						<?php echo '<a class="a-invert" href="'.$permalink.'" >'.$title.'</a>'; ?>
						<div class="lp-item-container-border clearfix">
							<?php echo '<div class="lp-item-info-container">'.$time.'</div>'; ?>
						</div>
					</div>
				</div>
				
				<?php echo '<div class="lp-item-text-container">'.$shortexcerpt.'</div>'; ?>
				<div class="lp-r-m-container">
					<a href="<?php the_permalink(); ?>" class="r-m-plus-small">READ MORE</a>
				</div>
			</li>

		  <?php endwhile; ?>
		</ul>
	</div>
</div>
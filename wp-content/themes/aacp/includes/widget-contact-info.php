<?php
// Add function to widgets_init
add_action( 'widgets_init', 'of_contact_widgets' );
add_filter('of_contact_widgets', 'do_shortcode');
// Register widget
function of_contact_widgets() {
	register_widget( 'of_contacT_Widget' );
}
// Widget class
class of_contact_widget extends WP_Widget {
	
function of_contacT_Widget() {

	// Widget settings
	$widget_ops = array(
		'classname' => 'of_contact_widget',
		'description' => __('Display contact information.', 'iwebtheme')
	);

	// Widget control settings
	$control_ops = array(
		'width' => 300,
		'height' => 350,
		'id_base' => 'of_contact_widget'
	);

	// Create the widget
	$this->WP_Widget( 'of_contact_widget', __('Contact info widget', 'iwebtheme'), $widget_ops, $control_ops );
	
}
	
function widget( $args, $instance ) {
	extract( $args );

	// Our variables from the widget settings
	$title = apply_filters('widget_title', $instance['title'] );
	$Address = $instance['Address'];
	$Phone = $instance['Phone'];
	$Phone2 = $instance['Phone2'];
	$Email = $instance['Email'];
	$Email2 = $instance['Email2'];

	// Before widget (defined by theme functions file)
	echo $before_widget;

	// Display the widget title if one was input
		$title = explode(' ', $title);
		if (count($title) > 1 ) {
		  $title[count($title)-1] = '<span class="bold">'.($title[count($title)-1]).'</span>';
		  $title = implode(' ', $title); 
		}
	if ( $title )
		echo $before_title . $title . $after_title;

	// Display contact info
	 ?>
	<ul class="footer-contact-info">
		<?php if ($Address!=='') { ?>
		<li class="footer-loc">
		<?php echo $Address; ?>
		</li>
		<?php } ?>
		<?php if ($Phone!=='') { ?>
		<li class="footer-phone">
		<?php echo $Phone; ?>
			<?php if ($Phone2!=='') { ?>
			<br><?php echo $Phone2; ?>
			<?php } ?>
		</li>
		<?php } ?>
		<?php if ($Email!=='') { ?>
		<li class="footer-mail">
		<?php echo $Email; ?>
			<?php if ($Email2!=='') { ?>
			<br><?php echo $Email2; ?>
			<?php } ?>
		</li>	
		<?php } ?>	
		
	</ul>		
	
	<?php
	// After widget (defined by theme functions file)
	echo $after_widget;	
}
	
function update( $new_instance, $old_instance ) {
	$instance = $old_instance;

	// Strip tags to remove HTML 
	$instance['title'] = strip_tags( $new_instance['title'] );
	$instance['Address'] = strip_tags( $new_instance['Address'] );
	$instance['Phone'] = strip_tags( $new_instance['Phone'] );
	$instance['Phone2'] = strip_tags( $new_instance['Phone2'] );
	$instance['Email'] = strip_tags( $new_instance['Email'] );
	$instance['Email2'] = strip_tags( $new_instance['Email2'] );
	return $instance;
}
	
	function form( $instance ) {
	
	// Set up some default widget settings
	$defaults = array(
			'title' => __( 'Contact info', 'iwebtheme'), 
			'Address' => __( 'Corporation, Inc. 123 Aolsom Ave, Suite 600 New York, CA 246012 ', 'iwebtheme' ),
			'Phone' => __( '(123) 456-7890', 'iwebtheme'), 
			'Phone2' => __( '(123) 987-6540', 'iwebtheme'), 
			'Email' => __( 'email@felius.com', 'iwebtheme'),
			'Email2' => __( 'email@optimas.com', 'iwebtheme')
	);

		$instance = wp_parse_args( (array)$instance, $defaults );
?>
	<p><label for="<?php echo $this->get_field_id( 'title' ) ?>"><?php _e( 'Widget Title', 'iwebtheme' ) ?></label>
    	<input type="text" class="widefat" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id( 'title' ) ?>" value="<?php echo $instance['title'] ?>"/></p>
	<p>
		<label for="<?php echo $this->get_field_id( 'Address' ); ?>"><?php _e('Address:', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'Address' ); ?>" name="<?php echo $this->get_field_name( 'Address' ); ?>" value="<?php echo $instance['Address']; ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'Phone' ); ?>"><?php _e('Phone (up to 2 phone numbers):', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'Phone' ); ?>" name="<?php echo $this->get_field_name( 'Phone' ); ?>" value="<?php echo $instance['Phone']; ?>" />
		<input class="widefat" id="<?php echo $this->get_field_id( 'Phone2' ); ?>" name="<?php echo $this->get_field_name( 'Phone2' ); ?>" value="<?php echo $instance['Phone2']; ?>" />
	</p>

	<p>
		<label for="<?php echo $this->get_field_id( 'Email' ); ?>"><?php _e('Email (up to 2 email address):', 'iwebtheme') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'Email' ); ?>" name="<?php echo $this->get_field_name( 'Email' ); ?>" value="<?php echo $instance['Email']; ?>" />
		<input class="widefat" id="<?php echo $this->get_field_id( 'Email2' ); ?>" name="<?php echo $this->get_field_name( 'Email2' ); ?>" value="<?php echo $instance['Email2']; ?>" />
	</p>

<?php
	}
}
?>
<?php
/**
 * @package WordPress
 */
?>
<div class="container clearfix m-top-60">
	
	<div class="jcarousel latest-posts-jc m-bot-50">
		<ul class="clearfix">

			<?php
				$args = array(
					'post_type' =>'documents',
					'document-type' => 'current-issue',
					'posts_per_page' => 1
				);
				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
				$documentuploaded = get_field('document_upload');
			?>
			<li class="four columns">
				<div class="hover-item">
					<div class="view view-first">
						<?php echo '<a href="'.$documentuploaded.'">'; ?><img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/members-home-journal.png" alt="" /></a>
					</div>
					<div class="lp-item-caption-container">
						<h5>Latest Journal</h5>
						<div class="lp-item-container-border clearfix">
						</div>
					</div>
				</div>
				<div class="lp-item-text-container">Download the latest edition of the Journal</div>
			</li>
			<?php endwhile; ?>



			<?php
				$args = array(
					'post_type' =>'page',
					'post__in' => array( 550 )
				);
				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
			?>
			<li class="four columns">
				<div class="hover-item">
					<div class="view view-first">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/members-home-research.png" alt="" /></a>
					</div>
					<div class="lp-item-caption-container">
						<h5><?php the_title(); ?></h5>
						<div class="lp-item-container-border clearfix">
						</div>
					</div>
				</div>
				<div class="lp-item-text-container">Take a look at the latest research from the AACP</div>
			</li>
			<?php endwhile; ?>



			<?php
				$args = array(
					'post_type' =>'page',
					'post__in' => array( 724 )
				);
				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
			?>
			<li class="four columns">
				<div class="hover-item">
					<div class="view view-first">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/members-home-offers.png" alt="" /></a>
					</div>
					<div class="lp-item-caption-container">
						<h5><?php the_title(); ?></h5>
						<div class="lp-item-container-border clearfix">
						</div>
					</div>
				</div>
				<div class="lp-item-text-container">Members only Special Offers from the AACP</div>
			</li>
			<?php endwhile; ?>



			<li class="four columns">
				<div class="hover-item">
					<div class="view view-first">
						<a href="http://www.jcm.co.uk/a-manual-of-acupuncture/ios/"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/members-home-deadman.png" alt="" /></a>
					</div>
					<div class="lp-item-caption-container">
						<h5>Deadman app</h5>
						<div class="lp-item-container-border clearfix">
						</div>
					</div>
				</div>
				<div class="lp-item-text-container">Visit the Manual of Acupuncture and download the app</div>
			</li>

		</ul>
	</div>
</div>
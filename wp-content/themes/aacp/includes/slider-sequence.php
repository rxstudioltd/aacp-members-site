<?php 
global $homepage;
global $post;
$old = $post;	
?>
<?php		
	$seq_template = get_post_meta($post->ID, 'iweb_seq_template', TRUE);
    $count = 1;
	$type = 'sequenceslider';
    $args=array(
	    'post_type' => $type,
		'posts_per_page' => -1,
		'meta_query' => array(
				 array(
					'key' => 'iweb_seq_template',
					'value' => $homepage
				 )
			  )
         );
	$counter = 0;
$seq_query = new WP_Query( $args  ); 
?>	
<?php if($homepage == 'Homepage 3') { ?>
<div id="slider-container" class="container clearfix">
	<div class="sixteen columns">
		<div id="sequence-theme">
			<div id="sequence">
				<ul>
					<?php if($seq_query->have_posts()): 	
					while($seq_query->have_posts()) : $seq_query->the_post();					
					
					$title = get_post_meta($post->ID, 'iweb_seq_title', TRUE);
					$title= explode(' ',$title);
					$title[0]='<span class="bold">'.$title[0].'</span>';
					$title=implode(' ',$title);
					
					$subtitle = get_post_meta($post->ID, 'iweb_seq_subtitle', TRUE);
					$subtitlefont35= explode(' ',$subtitle);
					$subtitlefont35[0]='<span class="font-35">'.$subtitlefont35[0].'</span>';
					$subtitlefont35=implode(' ',$subtitlefont35);					
					
					$subtitle2 = get_post_meta($post->ID, 'iweb_seq_subtitle2', TRUE);
					$subtitlefont52= explode(' ',$subtitle2);
					$subtitlefont52[0]='<span class="font-52">'.$subtitlefont52[0].'</span>';
					$subtitlefont52=implode(' ',$subtitlefont52);						
					$btntxt = get_post_meta($post->ID, 'iweb_seq_btntext', TRUE);
					$btnurl = get_post_meta($post->ID, 'iweb_seq_btnurl', TRUE);					
					
					$desc = get_post_meta($post->ID, 'iweb_seq_desc', TRUE);
					
					$bgimg = rwmb_meta( 'iweb_seq_imgbg', 'type=plupload_image' );
					foreach ( $bgimg as $image )
					{
					$seqbg = $image['full_url'];
					}
					$img = rwmb_meta( 'iweb_seq_img', 'type=plupload_image' );
					foreach ( $img as $image )
					{
					$seqimg = $image['full_url'];
					}
					
					$bg = get_post_meta($post->ID, 'iweb_seq_bgimage', TRUE);
					$img_w = '940';
					$img_h = '390';
					$seqbg = aq_resize($seqbg, $img_w, $img_h, true);	
					$seqimg = aq_resize($seqimg, $img_w, $img_h, true);	



                    ?>	
				
					<?php if($counter == 0) { ?>
						<li class="animate-in">
					<?php } else { ?>
						<li>
					<?php } ?>

						<?php if ($seqbg !='') { ?>
							<img class="bg-slide" src="<?php echo $seqbg; ?>" alt="" />
						<?php } ?>
						<?php if ($seqimg !='') { ?>
							<img class="model" src="<?php echo $seqimg; ?>" alt="" />
						<?php } ?>	
							<div class="title">
								<h2><?php echo $title; ?></h2>
								<p class="subtitle-2"><?php echo $desc; ?></p>
							</div>
							<div class="subtitle-3">
								<a class="button medium r-m-plus r-m-full" href="<?php echo $btnurl; ?>"><?php echo $btntxt; ?></a>
							</div>

						</li>
				
						<?php $counter++ ; endwhile; endif;
						$post = $old;
						?>						
					
				</ul>	
			</div>
			
				<div class="slider-nav-bg"></div>
					<ul class="nav">
						<?php if($seq_query->have_posts()): 	
						while($seq_query->have_posts()) : $seq_query->the_post(); 
						$bgimg = rwmb_meta( 'iweb_seq_imgbg', 'type=plupload_image' );
						foreach ( $bgimg as $image )
						{
						$seqbg = $image['full_url'];
						}
						
						$img = rwmb_meta( 'iweb_seq_img', 'type=plupload_image' );
						foreach ( $img as $image )
						{
						$seqimg = $image['full_url'];
						}

						$thumb_w = '130';
						$thumb_h = '54';
						$thumb = aq_resize($seqimg, $thumb_w, $thumb_h, true);	
						$thumb_bg = aq_resize($seqbg, $thumb_w, $thumb_h, true);	
						?>
						<li class="bw-wrapper"><img src="<?php echo $thumb; ?>" alt="Model 1" style="background: url(<?php echo $thumb_bg; ?>);" /></li>
						<?php endwhile; endif;
						$post = $old;
						?>
					</ul>
				<div class="arrows-nav ">
					<img class="prev slider1" src="<?php echo get_template_directory_uri('template_directory'); ?>/images/bt-prev.png" alt="Previous Frame" ><img class="next slider1" src="<?php echo get_template_directory_uri('template_directory'); ?>/images/bt-next.png" alt="Next Frame" >
				</div>
		</div>
	</div>
</div>
	<?php } ?>
	
	
<?php if($homepage == 'Homepage 4') { ?>
<div class="container m-bot-50">
	<div id="slider-container" class="sixteen columns content-container-white sequence-theme-6">
		<div id="sequence-theme">
			<div id="sequence" class="theme-2">
				<ul>
					<?php if($seq_query->have_posts()): 	
					while($seq_query->have_posts()) : $seq_query->the_post();					
					
					$title = get_post_meta($post->ID, 'iweb_seq_title', TRUE);
					$title= explode(' ',$title);
					$title[0]='<span class="bold">'.$title[0].'</span>';
					$title=implode(' ',$title);
					
					$subtitle = get_post_meta($post->ID, 'iweb_seq_subtitle', TRUE);
					$subtitlefont35= explode(' ',$subtitle);
					$subtitlefont35[0]='<span class="font-35">'.$subtitlefont35[0].'</span>';
					$subtitlefont35=implode(' ',$subtitlefont35);					
					
					$subtitle2 = get_post_meta($post->ID, 'iweb_seq_subtitle2', TRUE);
					$subtitlefont52= explode(' ',$subtitle2);
					$subtitlefont52[0]='<span class="font-52">'.$subtitlefont52[0].'</span>';
					$subtitlefont52=implode(' ',$subtitlefont52);						
					$btntxt = get_post_meta($post->ID, 'iweb_seq_btntext', TRUE);
					$btnurl = get_post_meta($post->ID, 'iweb_seq_btnurl', TRUE);					
					
					$desc = get_post_meta($post->ID, 'iweb_seq_desc', TRUE);

					$img = rwmb_meta( 'iweb_seq_img', 'type=plupload_image' );
					foreach ( $img as $image )
					{
					$seqimg = $image['full_url'];
					}
					
					$bg = get_post_meta($post->ID, 'iweb_seq_bgimage', TRUE);
					$img_w = '508';
					$img_h = '345';
					$slideimg = aq_resize($seqimg, $img_w, $img_h, true);	



                    ?>	
				
					<?php if($counter == 0) { ?>
						<li class="animate-in">
					<?php } else { ?>
						<li>
					<?php } ?>
						<h2 class="title"><?php echo $title; ?></h2>
						<h3 class="subtitle"><?php echo $subtitle; ?></h3>
						<p class="subtitle-2 slider-caption"><?php echo $desc; ?></p>
						<p class="subtitle-3">
							<a class="button get-theme" href="<?php echo $btnurl; ?>"><?php echo $btntxt; ?></a>
						</p>
						<?php if ($slideimg !='') { ?>
							<img class="model" src="<?php echo $slideimg; ?>" alt="" />
						<?php } ?>	

						</li>
				
						<?php $counter++ ; endwhile; endif;
						$post = $old;
						?>						
					
				</ul>	
			</div>

				<div class="arrows-nav ">
					<img class="prev slider1" src="<?php echo get_template_directory_uri('template_directory'); ?>/images/bt-prev.png" alt="Previous Frame" ><img class="next slider1" src="<?php echo get_template_directory_uri('template_directory'); ?>/images/bt-next.png" alt="Next Frame" >
				</div>
		</div>
	</div>
</div>
	<?php } ?>

<?php
/**
 * @package WordPress
 */
?>
<!-- LATEST WORK -->
<div class="container clearfix m-top-60">

		<div class="four columns carousel-intro m-bot-33">
			<div class="caption-container m-bot-20">
				<div class="title-block-text"><?php echo iwebtheme_smof_data('block_portfoliotitle'); ?></div>
				<div class="carousel-navi jcarousel-scroll">
								<div class="jcarousel-prev"></div>
								<div class="jcarousel-next"></div>
				</div>
			</div>
			
		</div>

	<div class="jcarousel latest-work-jc m-bot-30" >
		<ul class="clearfix">
        		    <?php					
                    $count = 1;
        		    $type = 'portfolio';
        		    $args=array(
        		    'post_type' => $type,
                    'posts_per_page' => -1
        		    );
        		    query_posts($args);	?>	
 

					<?php if (have_posts()) : while (have_posts()) : the_post();					
					$terms = ''; // variable
					$title=get_the_title();
					$title = wp_trim_words($title,$num_words =3);
					$title=explode(' ',$title);
					$title[0]='<span class="bold">'.$title[0].'</span>';
					$title=implode(' ',$title);
					
					if (has_post_thumbnail()) {					
						$thumb = get_post_thumbnail_id();
						$thumb_w = '460';
						$thumb_h = '272';
						$image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
						$image_url = $image_src [0];
						$attachment_url = wp_get_attachment_url($thumb, 'full');
						$image = aq_resize($attachment_url, $thumb_w, $thumb_h, true);							
					}			
        			
					
					$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'grid-thumb');
					$terms = get_the_terms( $post->ID, 'portfolio_categories' );
									foreach ( $terms as $term ) {
											$cats[0] = $term->name;
											$catname = join($cats);		
											$catname = preg_replace('/\s/', '', $catname);											
									}
                    ?>
		<!-- PORTFOLIO ITEM -->
			<li class="four columns">
				<div class="hover-item">

						<div class="view view-first">
							<img src="<?php echo $image; ?>" alt="<?php the_title(); ?>" />
							<div class="mask"></div>	
							<div class="abs">
								<a href="<?php echo $attachment_url; ?>" class="lightbox zoom info"></a><a href="<?php the_permalink(); ?>" class="link info"></a>
							</div>	
						</div>
						<div class="lw-item-caption-container">
							<a class="a-invert" href="<?php the_permalink(); ?>" >
							<div class="item-title-main-container clearfix">
								<div class="item-title-text-container">
							<?php echo $title; ?>
								</div></div>
							</a>
							<div class="item-caption"><?php echo $catname;?></div>
						</div>

				</div>
			</li>
  <?php endwhile; ?>	
		
			
	<?php endif; wp_reset_query(); ?>
	</ul>
	</div>
</div>
<?php
$attach_firstsection = get_post_meta($post->ID, 'iweb_first_customsections', TRUE);


	if( $attach_firstsection && $attach_firstsection !='' ){
		global $tmp_post;
		$tmp_post = $post;		
		$firstsection_post = get_post($attach_firstsection); 
		setup_postdata( $firstsection_post );
		$post = $firstsection_post;
		$bgsection_type = get_post_meta($post->ID, 'iweb_csection_bg', TRUE);
	$bgclass = '';
	if( $bgsection_type =='Transparent' ){
		$bgclass = 'no-bg';
	} else 	if( $bgsection_type =='White' ){
		$bgclass = 'white-bg';
	} else 	if( $bgsection_type =='Light grey' ){
		$bgclass = 'light-grey-bg';
	} else 	if( $bgsection_type =='Dark grey' ){
		$bgclass = 'dark-grey-bg';
	}
?>
<div class="<?php echo $bgclass; ?>">
<?php the_content(); ?>
</div>
<?php
	wp_reset_postdata();
	$post = $tmp_post;
}?>
<?php
$attach_secondsection = get_post_meta($post->ID, 'iweb_second_customsections', TRUE);

	if( $attach_secondsection && $attach_secondsection !='' ){
		global $tmp_post;
		$tmp_post = $post;		
		$secondsection_post = get_post($attach_secondsection); 
		setup_postdata( $secondsection_post );
		$post = $secondsection_post;
		$bgsection_type = get_post_meta($post->ID, 'iweb_csection_bg', TRUE);
		$bgclass = '';
	if( $bgsection_type =='Transparent' ){
		$bgclass = 'no-bg';
	} else 	if( $bgsection_type =='White' ){
		$bgclass = 'white-bg';
	} else 	if( $bgsection_type =='Light grey' ){
		$bgclass = 'light-grey-bg';
	} else 	if( $bgsection_type =='Dark grey' ){
		$bgclass = 'dark-grey-bg';
	}
?>
<div class="<?php echo $bgclass; ?>">
<?php the_content(); ?>
</div>
<?php
	wp_reset_postdata();
	$post = $tmp_post;
}?>
<?php
$attach_thirdsection = get_post_meta($post->ID, 'iweb_third_customsections', TRUE);


	if( $attach_thirdsection && $attach_thirdsection !='' ){
		global $tmp_post;
		$tmp_post = $post;		
		$thirdsection_post = get_post($attach_thirdsection); 
		setup_postdata( $thirdsection_post );
		$post = $thirdsection_post;
		$bgsection_type = get_post_meta($post->ID, 'iweb_csection_bg', TRUE);

		$bgclass = '';
	if( $bgsection_type =='Transparent' ){
		$bgclass = 'no-bg';
	} else 	if( $bgsection_type =='White' ){
		$bgclass = 'white-bg';
	} else 	if( $bgsection_type =='Light grey' ){
		$bgclass = 'light-grey-bg';
	} else 	if( $bgsection_type =='Dark grey' ){
		$bgclass = 'dark-grey-bg';
	}
?>
<div class="<?php echo $bgclass; ?>">
<?php the_content(); ?>
</div>
<?php
	wp_reset_postdata();
	$post = $tmp_post;
}?>
<?php
$attach_fourthsection = get_post_meta($post->ID, 'iweb_fourth_customsections', TRUE);

	if( $attach_fourthsection && $attach_fourthsection !='' ){
		global $tmp_post;
		$tmp_post = $post;		
		$fourthsection_post = get_post($attach_fourthsection); 
		setup_postdata( $fourthsection_post );
		$post = $fourthsection_post;
		$bgsection_type = get_post_meta($post->ID, 'iweb_csection_bg', TRUE);
		$bgclass = '';
	if( $bgsection_type =='Transparent' ){
		$bgclass = 'no-bg';
	} else 	if( $bgsection_type =='White' ){
		$bgclass = 'white-bg';
	} else 	if( $bgsection_type =='Light grey' ){
		$bgclass = 'light-grey-bg';
	} else 	if( $bgsection_type =='Dark grey' ){
		$bgclass = 'dark-grey-bg';
	}
?>
<div class="<?php echo $bgclass; ?>">
<?php the_content(); ?>
</div>
<?php
	wp_reset_postdata();
	$post = $tmp_post;
}?>
<?php
/**
 * @package WordPress
 */
?>
<!-- OUR CLIENTS -->	
	<div class="container clearfix">
		<div class="sixteen columns m-bot-20">
			<ul class="our-clients-container clearfix">
        		    <?php					
                    $count = 1;
        		    $type = 'client';
        		    $args=array(
        		    'post_type' => $type,
                    'posts_per_page' => -1
        		    );
        		    query_posts($args);	?>	
					
					<?php if (have_posts()) : while (have_posts()) : the_post();					
						$clienturl = get_post_meta($post->ID, 'iweb_client_link', TRUE);
						$thumb_w = '188';
						$thumb_h = '80';
						$client_img = rwmb_meta( 'iweb_client_img', 'type=plupload_image' );
						foreach ( $client_img as $image )
						{
							$client_img = $image['full_url'];
							$client_img = aq_resize($client_img, $thumb_w, $thumb_h, true);	
						}
                    ?>
					
				<li class="">
					<a href="<?php echo $clienturl; ?>">
						<div class="bw-wrapper">
							<img src="<?php echo $client_img; ?>" alt="<?php the_title(); ?>" />
						</div>
					</a>
				</li>
				<?php endwhile; ?>
            
			<?php endif; wp_reset_query(); ?>
			</ul>
		</div>	
	</div>
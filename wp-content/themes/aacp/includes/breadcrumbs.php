<?php function get_breadcrumbs()
{
    global $wp_query;
    if ( !is_home() ){
        // Start the UL
        echo '<h1 class="page-title">';
		echo '<a href="'. home_url() .'">'. 'Home ' .'</a>';
        // Add the Home link
        if ( is_category() )
        {
            $catTitle = single_cat_title( "", false );
            $cat = get_cat_ID( $catTitle );
            echo get_category_parents( $cat, TRUE, " " );
        }
        elseif ( is_archive() && !is_category() )
        {
            echo ' <span class="bread_arrow">/ </span><span class="current_crumb">Archives</span>';
        }
        elseif ( is_search() ) {
 
            echo ' <span class="bread_arrow">/ </span><span class="current_crumb">'.__('Search Results','iwebtheme').'</span>';
        }
        elseif ( is_404() )
        {
            echo ' <span class="bread_arrow">/ </span><span class="current_crumb">'.__('404 Not Found','iwebtheme').'</span>';
        }
		
        elseif ( is_singular( 'portfolio' ) )
        {
			echo '<span class="bread_arrow"> / </span>';
            echo the_title('','', FALSE) ."";
        }
		
        elseif ( is_single() )
        {
		
            $categories = get_the_category();
			$category_id ='';
			foreach($categories as $category) {
				if ($category->cat_name!== 'broder') {
					$category_id = get_cat_ID( $category->cat_name );
				}
			}
			echo '<span class="bread_arrow"> / </span>';
            echo get_category_parents( $category_id, TRUE, ' <span class="bread_arrow">/</span> ' );
			
            echo the_title('','', FALSE) ."</span>";
        }

        elseif ( is_page() )
        {
            $post = $wp_query->get_queried_object();
 
            if ( $post->post_parent == 0 ){
 
                echo ' <span class="bread_arrow">/</span> <span class="current_crumb">'.the_title('','', FALSE).'</span>';
 
            } else {
                $title = the_title('','', FALSE);
                $ancestors = array_reverse( get_post_ancestors( $post->ID ) );
                array_push($ancestors, $post->ID);
 
                foreach ( $ancestors as $ancestor ){
                    if( $ancestor != end($ancestors) ){
                        echo ' <span class="bread_arrow">/</span><span class="current_crumb"><a href="'. get_permalink($ancestor) .'">'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</a></span>';
                    } else {
                        echo ' <span class="bread_arrow">/</span><span class="current_crumb">'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</span>';
                    }
                }
            }
        }
        // End the UL
        echo "</h1>";
    }
}
?>
<?php
return get_breadcrumbs();
?>
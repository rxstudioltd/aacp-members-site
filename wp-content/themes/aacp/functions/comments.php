<?php
function custom_comments($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>

	<li id="comment-<?php comment_ID() ?>" class="comment">
		<div class="single-comment">
		
			<div class="comment-avatar">
				<?php echo get_avatar($comment,$size='92'); ?>
			</div>
			
            <div class="comment-head clearfix">
				<div class="comment-name left"><?php echo get_comment_author_link(); ?></div>
				<div class="right">
					<span class="comment-desc"><?php echo get_comment_date(); ?></span>
					<span class="comment-reply"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
				</div>  
			
            </div><!-- END comment_meta-->
			
			<div class="comment-text">
				<?php if ($comment->comment_approved == '0') : ?>
							<em><?php _e('Your comment is awaiting moderation.', 'iwebtheme') ?></em>
						<?php endif; ?>	
				<?php comment_text() ?>
			</div>
			

		</div>
<?php
} ?>
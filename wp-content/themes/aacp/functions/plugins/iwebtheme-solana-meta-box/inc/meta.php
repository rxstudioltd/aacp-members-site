<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = 'iweb_';

global $meta_boxes;

$meta_boxes = array();

/* =========================================*/
// Page meta
/* =========================================*/

$meta_boxes[] = array(
	'id' => 'toppageattach',
	'title' => 'Attach custom sections (Before content)',
	'pages' => array( 'page' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'    => __( 'Top custom section (above content - under header/slider)', 'rwmb' ),
			'id'      => "{$prefix}top_customsections",
			'type'    => 'post',

			// Post type
			'post_type' => 'customsections',
			// Field type, either 'select' or 'select_advanced' (default)
			'field_type' => 'select_advanced',
			// Query arguments (optional). No settings means get all published posts
			'query_args' => array(
				'post_status' => 'publish',
				'posts_per_page' => '-1',
			),
			'std'         => '', // Default value, optional
			'placeholder' => __( 'Select section', 'rwmb' ),
		),

	)
);


$meta_boxes[] = array(
	'id' => 'bottompageattach',
	'title' => 'Attach custom sections (After content)',
	'pages' => array( 'page' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'    => __( 'First custom section', 'rwmb' ),
			'id'      => "{$prefix}first_customsections",
			'type'    => 'post',

			// Post type
			'post_type' => 'customsections',
			// Field type, either 'select' or 'select_advanced' (default)
			'field_type' => 'select_advanced',
			// Query arguments (optional). No settings means get all published posts
			'query_args' => array(
				'post_status' => 'publish',
				'posts_per_page' => '-1',
			),			
			'std'         => '', // Default value, optional
			'placeholder' => __( 'Select section', 'rwmb' ),
		),
		array(
			'name'    => __( 'Second custom section', 'rwmb' ),
			'id'      => "{$prefix}second_customsections",
			'type'    => 'post',

			// Post type
			'post_type' => 'customsections',
			// Field type, either 'select' or 'select_advanced' (default)
			'field_type' => 'select_advanced',
			// Query arguments (optional). No settings means get all published posts
			'query_args' => array(
				'post_status' => 'publish',
				'posts_per_page' => '-1',
			),
			'std'         => '', // Default value, optional
			'placeholder' => __( 'Select section', 'rwmb' ),
		),
		array(
			'name'    => __( 'Third custom section', 'rwmb' ),
			'id'      => "{$prefix}third_customsections",
			'type'    => 'post',

			// Post type
			'post_type' => 'customsections',
			// Field type, either 'select' or 'select_advanced' (default)
			'field_type' => 'select_advanced',
			// Query arguments (optional). No settings means get all published posts
			'query_args' => array(
				'post_status' => 'publish',
				'posts_per_page' => '-1',
			),
			'std'         => '', // Default value, optional
			'placeholder' => __( 'Select section', 'rwmb' ),
			
		),	
		array(
			'name'    => __( 'Fourth custom section', 'rwmb' ),
			'id'      => "{$prefix}fourth_customsections",
			'type'    => 'post',

			// Post type
			'post_type' => 'customsections',
			// Field type, either 'select' or 'select_advanced' (default)
			'field_type' => 'select_advanced',
			// Query arguments (optional). No settings means get all published posts
			'query_args' => array(
				'post_status' => 'publish',
				'posts_per_page' => '-1',
			),
			'std'         => '', // Default value, optional
			'placeholder' => __( 'Select section', 'rwmb' ),
			
		),			
	)
);



$meta_boxes[] = array(
	'id' => 'pageoptions',
	'title' => 'Page settings',
	'pages' => array( 'page' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
				'name'		=> 'Page title',
				'id'		=> $prefix . "page_title",
				'type'		=> 'text',
				'std'		=> 'Page title',
				'desc'		=> 'Enter your page title (leave it blank for no page title)',
		),	
		array(
			'name'     => __( 'Show latest portfolio block', 'rwmb' ),
			'id'       => "{$prefix}page_portfolio",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Enable' => __( 'Enable', 'rwmb' ),
				'Disable' => __( 'Disable', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select enable to show signup newsletter block below page content otherwise select disable',
			'placeholder' => __( 'Select', 'rwmb' ),
		),	
		array(
			'name'     => __( 'Show signup newsletter block', 'rwmb' ),
			'id'       => "{$prefix}page_signup",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Enable' => __( 'Enable', 'rwmb' ),
				'Disable' => __( 'Disable', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select enable to show signup newsletter block below page content otherwise select disable',
			'placeholder' => __( 'Select', 'rwmb' ),
		),		
		array(
			'name'     => __( 'Show client block', 'rwmb' ),
			'id'       => "{$prefix}page_clients",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Enable' => __( 'Enable', 'rwmb' ),
				'Disable' => __( 'Disable', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select enable to show clients block below page content otherwise select disable',
			'placeholder' => __( 'Select', 'rwmb' ),
		),			
	)
);

// ================================== end page meta box


$meta_boxes[] = array(
	'id' => 'pageoptions',
	'title' => 'Single post settings',
	'pages' => array( 'post' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
				'name'		=> 'Single post title',
				'id'		=> $prefix . "post_title",
				'type'		=> 'text',
				'std'		=> 'Page title',
				'desc'		=> 'Enter your page title (leave it blank for no page title)',
		),	
		array(
			'name'     => __( 'Show post author box', 'rwmb' ),
			'id'       => "{$prefix}post_author",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Enable' => __( 'Enable', 'rwmb' ),
				'Disable' => __( 'Disable', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select enable to show post author box after post content otherwise select disable',
			'placeholder' => __( 'Select', 'rwmb' ),
		),		
	)
);

// ================================== end post meta box

/* =========================================*/
// Custom section meta
/* =========================================*/

$meta_boxes[] = array(
	'id' => 'pageoptions',
	'title' => 'Custom section setting',
	'pages' => array( 'customsections' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'     => __( 'Section background', 'rwmb' ),
			'id'       => "{$prefix}csection_bg",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Transparent' => __( 'Transparent', 'rwmb' ),
				'White' => __( 'White', 'rwmb' ),
				'Light grey' => __( 'Light grey', 'rwmb' ),
				'Dark grey' => __( 'Dark grey', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select background type for this custom section',
			'placeholder' => __( 'Section background', 'rwmb' ),
		),			
	)
);

// ================================== end custom section meta box

/* =========================================*/
// client meta
/* =========================================*/

$meta_boxes[] = array(
	'id' => 'pageoptions',
	'title' => 'Client setting',
	'pages' => array( 'client' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
				'name'		=> 'Client link URL',
				'id'		=> $prefix . "client_link",
				'type'		=> 'text',
				'std'		=> 'http://somesite.com',
				'desc'		=> 'Enter client website URL',
		),	
		array(
			'name' => 'Client image (min 188 x 80 - width height in px)',
			'id'   => "{$prefix}client_img",
			'type' => 'plupload_image',
			'desc' => 'Select or upload your client image',
			'max_file_uploads' => 1,
		),		
	)
);

// ================================== end client meta box

/* =========================================*/
// Flexslider meta
/* =========================================*/

$meta_boxes[] = array(
	'id' => 'pageoptions',
	'title' => 'Section options',
	'pages' => array( 'flexslider' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'     => __( 'Assign this slider item to homepage template', 'rwmb' ),
			'id'       => "{$prefix}flexslider_template",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Homepage 1' => __( 'Homepage 1', 'rwmb' ),
				'Homepage 2' => __( 'Homepage 2', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select homepage template where this slider item will be used, this slider item will be assigned on selected homepage template only',
			'placeholder' => __( 'Home page template', 'rwmb' ),
		),

		array(
				'name'		=> 'Slider title',
				'id'		=> $prefix . "flexslider_title",
				'type'		=> 'text',
				'std'		=> 'Flexslider title',
				'desc'		=> 'Enter your Flexslider title',
		),
		array(
				'name'		=> 'Slider caption',
				'id'		=> $prefix . "flexslider_caption",
				'type'		=> 'textarea',
				'std'		=> 'CRAS JUSTO ODIO, DAPIBUS AC FACILISIS',
				'desc'		=> 'Enter your Flexslider caption',
		),
		array(
				'name'		=> 'Slider button text',
				'id'		=> $prefix . "flexslider_btntext",
				'type'		=> 'text',
				'std'		=> 'READ MORE',
				'desc'		=> 'Enter your Flexslider button text',
		),
		array(
				'name'		=> 'Slider button link URL',
				'id'		=> $prefix . "flexslider_btnurl",
				'type'		=> 'text',
				'std'		=> 'http://themeforest.net/user/iWebStudio/portfolio',
				'desc'		=> 'Enter your Flexslider button link URL',
		),
		// flexslider image
		array(
			'name' => 'Flexslider background image (Used for Homepage 1)',
			'id'   => "{$prefix}flexslider_imgbg",
			'type' => 'plupload_image',
			'desc' => 'Select or upload your image and set as slider image',
			'max_file_uploads' => 1,
		),
		array(
			'name' => 'Flexslider image',
			'id'   => "{$prefix}flexslider_img",
			'type' => 'plupload_image',
			'desc' => 'Select or upload your image and set as slider image',
			'max_file_uploads' => 1,
		),
		
	)
);

// ================================== end flexslider meta box


/* =========================================*/
// Sequence slider meta
/* =========================================*/

$meta_boxes[] = array(
	'id' => 'sequenceoptions',
	'title' => 'Sequence slider item',
	'pages' => array( 'sequenceslider' ),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
			'name'     => __( 'Assign this slider item to homepage template', 'rwmb' ),
			'id'       => "{$prefix}seq_template",
			'type'     => 'select_advanced',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => array(
				'Homepage 3' => __( 'Homepage 3', 'rwmb' ),
				'Homepage 4' => __( 'Homepage 4', 'rwmb' ),
			),
			'multiple'    => false,
			'desc'		=> 'Select homepage template where this slider item will be used, this slider item will be assigned on selected homepage template only',
			'placeholder' => __( 'Home page template', 'rwmb' ),
		),

		array(
				'name'		=> 'Slider title',
				'id'		=> $prefix . "seq_title",
				'type'		=> 'text',
				'std'		=> 'LOREM IPSUM',
				'desc'		=> 'Enter your sequence slider title',
		),
		array(
				'name'		=> 'Slider subtitle (Used on Homepage 4 only)',
				'id'		=> $prefix . "seq_subtitle",
				'type'		=> 'textarea',
				'std'		=> 'Multi-purpose Creative Business',
				'desc'		=> 'Enter your sequence slider subtitle',
		),
		array(
				'name'		=> 'Slider description/caption',
				'id'		=> $prefix . "seq_desc",
				'type'		=> 'textarea',
				'std'		=> 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.',
				'desc'		=> 'sequence slider short paragraph of description',
		),
		array(
				'name'		=> 'Slider button text',
				'id'		=> $prefix . "seq_btntext",
				'type'		=> 'text',
				'std'		=> 'READ MORE',
				'desc'		=> 'Enter your Flexslider button text',
		),
		array(
				'name'		=> 'Slider button link URL',
				'id'		=> $prefix . "seq_btnurl",
				'type'		=> 'text',
				'std'		=> 'http://themeforest.net/user/iWebStudio/portfolio',
				'desc'		=> 'Enter your Flexslider button link URL',
		),
		// flexslider image
		array(
			'name' => 'Sequence slider background image (Used on Homepage 3 only)',
			'id'   => "{$prefix}seq_imgbg",
			'type' => 'plupload_image',
			'desc' => 'Select or upload your image and set as sequence slider background image',
			'max_file_uploads' => 1,
		),
		array(
			'name' => 'Sequence slider image',
			'id'   => "{$prefix}seq_img",
			'type' => 'plupload_image',
			'desc' => 'Select or upload your image and set as slider image',
			'max_file_uploads' => 1,
		),
		
	)
);

// ================================== end sequence slider meta box


/* =========================================*/
// Custom section meta
/* =========================================*/
$meta_boxes[] = array(
	'id' => 'customsections_meta',
	'title' => 'Testimonial',
	'pages' => array( 'testimonial'),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
				'name'		=> 'Testimonial text',
				'id'		=> $prefix . "testomonial_txt",
				'type'		=> 'textarea',
				'std'		=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisl orci, condim entum ultrices consequat eu, vehicula. Aenean nisl orci, condim.',
				'desc'		=> 'Enter testimonial text',
		),
		array(
				'name'		=> 'Testimonial author',
				'id'		=> $prefix . "testomonial_author",
				'type'		=> 'text',
				'std'		=> 'John Doe',
				'desc'		=> 'Enter testimonial author',
		),
		array(
				'name'		=> 'Occupation',
				'id'		=> $prefix . "testomonial_occu",
				'type'		=> 'text',
				'std'		=> 'CEO, Company',
				'desc'		=> 'Enter Occupation',
		),
	)
);



/* =========================================*/
// Portfolio meta
/* =========================================*/
$meta_boxes[] = array(
	'id' => 'portfolio_info',
	'title' => 'Portfolio detail options',
	'pages' => array( 'portfolio'),
	'context' => 'normal',
	'priority' => 'high',

	// List of meta fields
	'fields' => array(
		array(
				'name'		=> 'Project overview',
				'id'		=> $prefix . "project_overview",
				'type'		=> 'textarea',
				'std'		=> 'Vestibulum pulvinar adipiscing turpis vitae at ultrices. Suspendisse eu lectus dui, vitae lob in ortis lorem convallis semper felis. Fusce ravida nibh et ante accusan molestie.',
				'desc'		=> 'Short paragraph of project overview',
		),
		array(
				'name'		=> 'Client name',
				'id'		=> $prefix . "project_client",
				'type'		=> 'text',
				'std'		=> 'http://themeforest.net',
				'desc'		=> 'Enter project client name',
		),	
		array(
				'name'		=> 'Project link URL',
				'id'		=> $prefix . "project_link",
				'type'		=> 'text',
				'std'		=> 'http://somesite.com',
				'desc'		=> 'Enter project URL',
		),		
		array(
				'name'		=> 'Project link button',
				'id'		=> $prefix . "project_btn",
				'type'		=> 'text',
				'std'		=> 'Online',
				'desc'		=> 'Enter project link button text',
		),	
	)
);


// ============================================= *//



/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function YOUR_PREFIX_register_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Meta_Box' ) )
		return;

	global $meta_boxes;
	foreach ( $meta_boxes as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'YOUR_PREFIX_register_meta_boxes' );
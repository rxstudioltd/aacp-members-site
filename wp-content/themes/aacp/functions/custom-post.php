<?php
/*============================================
Custom post type
=============================================*/

// register homepage sections cpt
add_action( 'init', 'iwebtheme_post_type_customsections' );

function iwebtheme_post_type_customsections() {

    $labels = array( 
        'name' => _x( 'Custom sections', 'customsections', 'iwebtheme' ),
        'singular_name' => _x( 'Custom section', 'customsections', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'customsections', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Custom section', 'customsections', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Custom section', 'customsections', 'iwebtheme' ),
        'new_item' => _x( 'New Custom section', 'customsections', 'iwebtheme' ),
        'view_item' => _x( 'View Custom section', 'customsections', 'iwebtheme' ),
        'search_items' => _x( 'Search Custom section', 'customsections', 'iwebtheme' ),
        'not_found' => _x( 'No Custom section found', 'customsections', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No Custom section found in Trash', 'customsections', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Custom section:', 'customsections', 'iwebtheme' ),
        'menu_name' => _x( 'Custom sections', 'customsections', 'iwebtheme' ),
		'all_items' => _x( 'All Custom sections', 'customsections', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
		'menu_icon' => get_template_directory_uri() . '/images/admin/ui-custom.png'
    );

    register_post_type( 'customsections', $args );
}

 
/* ================================= end sequnce slider ================================*/

// register flexslider cpt
add_action( 'init', 'iwebtheme_post_type_flexslider' );

function iwebtheme_post_type_flexslider() {

    $labels = array( 
        'name' => _x( 'Flexsliders', 'flexslider', 'iwebtheme' ),
        'singular_name' => _x( 'Flexslider', 'flexslider', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'flexslider', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Flexslider slider', 'flexslider', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Flexslider slider', 'flexslider', 'iwebtheme' ),
        'new_item' => _x( 'New Flexslider slider', 'flexslider', 'iwebtheme' ),
        'view_item' => _x( 'View Flexslider slider', 'flexslider', 'iwebtheme' ),
        'search_items' => _x( 'Search Flexslider slider', 'flexslider', 'iwebtheme' ),
        'not_found' => _x( 'No flexslider slider found', 'flexslider', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No flexslider slider found in Trash', 'flexslider', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Flexslider slider:', 'flexslider', 'iwebtheme' ),
        'menu_name' => _x( 'Flexslider sliders', 'flexslider', 'iwebtheme' ),
		'all_items' => _x( 'All Flexslider sliders', 'flexslider', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
		'menu_icon' => get_template_directory_uri() . '/images/admin/icon-slider.png',
        'capability_type' => 'post'
    );

    register_post_type( 'flexslider', $args );
}


// register case studies cpt
add_action( 'init', 'post_type_casestudies' );

function post_type_casestudies() {

    $labels = array( 
        'name' => _x( 'Case Studies', 'casestudies', 'iwebtheme' ),
        'singular_name' => _x( 'Case Studies', 'casestudies', 'iwebtheme' ),
        'add_new' => _x( 'Add New', 'casestudies', 'iwebtheme' ),
        'add_new_item' => _x( 'Add New Case Study', 'casestudies', 'iwebtheme' ),
        'edit_item' => _x( 'Edit Case Study', 'casestudies', 'iwebtheme' ),
        'new_item' => _x( 'New Case Study', 'casestudies', 'iwebtheme' ),
        'view_item' => _x( 'View Case Study', 'casestudies', 'iwebtheme' ),
        'search_items' => _x( 'Search Case Studies', 'casestudies', 'iwebtheme' ),
        'not_found' => _x( 'No Case Studies found', 'casestudies', 'iwebtheme' ),
        'not_found_in_trash' => _x( 'No Case Studies found in Trash', 'casestudies', 'iwebtheme' ),
        'parent_item_colon' => _x( 'Parent Case Studies:', 'casestudies', 'iwebtheme' ),
        'menu_name' => _x( 'Case Studies', 'casestudies', 'iwebtheme' ),
        'all_items' => _x( 'All Case Studies', 'casestudies', 'iwebtheme' )
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'supports' => array( 'title', 'editor' ),
        'public' => true,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
        'menu_icon' => get_template_directory_uri() . '/images/admin/ui-custom.png'
    );

    register_post_type( 'casestudies', $args );
}


/* Create the Documents Custom Post Type ------------------------------------------*/
function create_post_type_documents() 
{
    $labels = array(
        'name' => __( 'Documents' ),
        'singular_name' => __( 'Document' ),
        'add_new' => __('Add New'),
        'add_new_item' => __('Add New Document'),
        'edit_item' => __('Edit Document'),
        'new_item' => __('New Document'),
        'view_item' => __('View Document'),
        'search_items' => __('Search Document'),
        'not_found' =>  __('No Document found'),
        'not_found_in_trash' => __('No Document found in Trash'), 
        'parent_item_colon' => ''
      );
      
      $args = array(
        'labels' => $labels,
        'public' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        // Uncomment the following line to change the slug; 
        // You must also save your permalink structure to prevent 404 errors
        'rewrite' => array( 'slug' => 'Documents' ), 
        'supports' => array('title','editor','thumbnail','page-attributes'),
        'menu_icon' => get_template_directory_uri() . '/images/admin/ui-custom.png'
      ); 
      
      register_post_type(__( 'documents' ),$args);
}

/* Create the Document Type Taxonomy --------------------------------------------*/
function build_taxonomies_document(){
    $labels = array(
        'name' => __( 'Document Type' ),
        'singular_name' => __( 'Document Type' ),
        'search_items' =>  __( 'Search Document Types' ),
        'popular_items' => __( 'Popular Document Types' ),
        'all_items' => __( 'All Document Types' ),
        'parent_item' => __( 'Parent Document Type' ),
        'parent_item_colon' => __( 'Parent Document Type:' ),
        'edit_item' => __( 'Edit Document Type' ), 
        'update_item' => __( 'Update Document Type' ),
        'add_new_item' => __( 'Add New Document Type' ),
        'new_item_name' => __( 'New Document Type Name' ),
        'separate_items_with_commas' => __( 'Separate Document types with commas' ),
        'add_or_remove_items' => __( 'Add or remove Document types' ),
        'choose_from_most_used' => __( 'Choose from the most used Document types' ),
        'menu_name' => __( 'Document Types' )
    );
    
    register_taxonomy(
        'document-type', 
        array( __( 'documents' )), 
        array(
            'hierarchical' => true, 
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'document-type', 'hierarchical' => true)
        )
    );
    
}

add_action( 'init', 'create_post_type_documents' );
add_action( 'init', 'build_taxonomies_document', 0 );



?>
<?php
/*
 * Custom functions to be used inside themes.
 *
 */
// post views ======================================================

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


// =================================================================
/* clean shortcode */
function clean_shortcodes($content) {   
    $array = array (
        '<p>[' => '[', 
        ']</p>' => ']',
        '<p><span>[' => '[', 
        ']</span></p>' => ']', 
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}
add_filter('the_content', 'clean_shortcodes');
// =================================================================

	
// Mailchimp ===========================================================

function mailchimp_add() {

	$mc_api = iwebtheme_smof_data('mc_api');
	$mc_listid = iwebtheme_smof_data('mc_listid');

	$email = ( isset( $_POST['email'] ) ) ? $_POST['email'] : '';
	$fname = ( isset( $_POST['fname'] ) ) ? $_POST['fname'] : '';
	
	if (!empty($fname)) {
		$merge_vars = array('FNAME' => $fname);
	}
	
	if(empty($email) || !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $email)) {
		echo '<script type="text/javascript">
			jQuery("input#email").focus();
		</script>';
		die();
	}
	
	// require mailchimp php class
	require_once('MCAPI.class.php');

	$api = new MCAPI($mc_api);
	
	if($api->listSubscribe($mc_listid, $email, $merge_vars) === true) {
		echo '<script type="text/javascript">
			jQuery("form#mailchimp").fadeOut();
		</script>
		<span class="mailchimp_success">
			<strong>Success!</strong> Check your email to confirm!
		</span>';
		die();
	} else {
		echo "Error! " .$api->errorMessage;
		die();
	}
}
add_action('wp_ajax_mailchimp_add', 'mailchimp_add');
add_action('wp_ajax_nopriv_mailchimp_add', 'mailchimp_add');

	
// Pagination ==========================================================

function pagination($pages = '', $range = 1)
{
     $showitems = ($range * 2)+1; 
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }  
 
     if(1 != $pages)
     {
         echo "<ul class=\"pagination-1\">";
         if($paged > 1 && $showitems < $pages) echo "<li><a class=\"pag-prev\" href='".get_pagenum_link($paged - 1)."'></a></li>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li><a class=\"pag-current\" href=\"#\">".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<li><a class=\"pag-next\" href=\"".get_pagenum_link($paged + 1)."\"></a></li>";

         echo "</ul>\n";
     }
}
?>
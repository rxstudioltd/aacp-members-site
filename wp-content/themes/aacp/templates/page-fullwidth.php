<?php
/*
Template Name: Fullwidth page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.white-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg);
		height:280px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>

</div>	<!-- Grey bg end -->
<div class="search-area-holder">
	<div class="container m-bot-35 clearfix">
		<?php dynamic_sidebar('Search Area'); ?>
	</div>
</div>
<div class="members-menu-holder">
	<div class="container m-bot-35 clearfix">
		<?php 
			echo do_shortcode( '[su_members class="members-menu-alert"][su_menu name="Members Menu" class="sf-menu clearfix"][/su_members]' );
		?>
	</div>
</div>
<div class="container m-bot-35 clearfix">
		<div class="sixteen columns  main-pages">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?> 
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
		</div>	
</div>
<?php get_template_part('includes/part-custom-bottom'); ?>
<?php if($mb_portfolio == 'Enable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>

<?php get_template_part( 'includes/part-newsletter' ); ?>

<?php if($mb_clients == 'Enable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>	
<?php get_footer(); ?>
<?php
/*
Template Name: Blog page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.white-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg);
		height:280px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>
</div>	<!-- Grey bg end -->
<div class="search-area-holder">
	<div class="container m-bot-35 clearfix">
		<?php dynamic_sidebar('Search Area'); ?>
	</div>
</div>
<div class="members-menu-holder">
	<div class="container m-bot-35 clearfix">
		<?php 
			echo do_shortcode( '[su_members class="members-menu-alert"][su_menu name="Members Menu" class="sf-menu clearfix"][/su_members]' );
		?>
	</div>
</div>

<div class="container clearfix">

		<div class="sixteen columns">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?> 
			<h2><?php the_title(); ?></h2>
			<?php endwhile; ?>
			<?php endif; ?>
		</div>	
		
		<div class="sixteen columns m-bot-25">
		<?php query_posts('post_type=post&cat=49&post_status=publish&paged='. get_query_var('paged')); ?>
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?> 
			<?php setPostViews(get_the_ID()); ?>			
			<?php get_template_part( 'includes/content', get_post_format() ); ?>
					<?php endwhile; ?>

			<?php if (function_exists("pagination")) { ?>
			<div class="pagination-1-container">
			<?php pagination(); ?>
			</div>
			<?php } else {
			posts_nav_link(' &#183; ', 'previous page', 'next page'); 	
			} ?>
		
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		</div>	
	

</div>
<!-- end of section -->

<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>

<?php get_footer(); ?>
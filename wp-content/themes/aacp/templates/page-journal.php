<?php
/*
Template Name: Journal page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.white-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg);
		height:280px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>

</div>	<!-- Grey bg end -->
<div class="search-area-holder">
	<div class="container m-bot-35 clearfix">
		<?php dynamic_sidebar('Search Area'); ?>
	</div>
</div>
<div class="members-menu-holder">
	<div class="container m-bot-35 clearfix">
		<?php 
			echo do_shortcode( '[su_members class="members-menu-alert"][su_menu name="Members Menu" class="sf-menu clearfix"][/su_members]' );
		?>
	</div>
</div>
<div class="container m-bot-35 clearfix">

		<div class="sixteen columns main-pages">

			<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?> 
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				<?php endwhile; ?>
			<?php endif; ?>

			&nbsp;
			<hr>
			<h3>Current Issue</h3>
			<ul class="journal-archive-holder">
				<?php
					$args = array(
						'post_type' =>'documents',
						'document-type' => 'current-issue',
						'post_per_page' => 1,
					);
					$query = new WP_Query($args);
					while ($query->have_posts()) : $query->the_post();
				?>
					<?php if( get_field('document_upload') ):?>
						<?php 
						$documentuploaded = get_field('document_upload');
						$documenttitle = get_the_title(); 
						echo do_shortcode( '[su_members message="This content is for registered users only. Please %login%." color="#ffcc00" login_text="login" login_url="/wp-login.php" class=""]<li class="journal-archive-item">
							<img src="/wp-content/uploads/2013/10/icon-document.png">
							<h6>'.$documenttitle.'</h6>
							<br />
							<a class="blue-button" href="'.$documentuploaded.'">DOWNLOAD</a>
						</li>[/su_members]' ); 
						?>
	            		
					<?php endif; ?>
				<?php endwhile; ?>
			</ul>

			&nbsp;
			<hr>
			<h3>Recent Back Issues</h3>

			<ul class="journal-archive-holder">
				<?php
					$args = array(
						'post_type' =>'documents',
						'document-type' => 'recent-back-issues',
						'post_per_page' => 1,
					);
					$query = new WP_Query($args);
					while ($query->have_posts()) : $query->the_post();
					
					if( get_field('document_upload') ):
						$documentuploaded = get_field('document_upload');
						$documenttitle = get_the_title(); 
						echo do_shortcode( '[su_members message="This content is for registered users only. Please %login%." color="#ffcc00" login_text="login" login_url="/wp-login.php" class=""]<li class="journal-archive-item">
							<img src="/wp-content/uploads/2013/10/icon-document.png">
							<h6>'.$documenttitle.'</h6>
							<br />
							<a class="blue-button" href="'.$documentuploaded.'">DOWNLOAD</a>
						</li>[/su_members]' );
						?>
	            		
					<?php endif; ?>
				<?php endwhile; ?>
			</ul>
			
			&nbsp;
			<hr>
			<h3>Public Journal Archive</h3>
			<ul class="journal-archive-holder">
			<?php
				$args = array(
					'post_type' =>'documents',
					'document-type' => 'archives',
					'orderby' => 'title',
					'order' => 'DESC',
					'post_per_page' => -1,
				);
				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
			?>
				<?php if( get_field('document_upload') ):?>
            		<li class="journal-archive-item">
						<img src="/wp-content/uploads/2013/10/icon-document.png">
						<h6><?php the_title(); ?></h6>
						<br />
						<a class="blue-button" href="<?php the_field('document_upload'); ?>">DOWNLOAD</a>
					</li>
				<?php endif; ?>
			<?php endwhile; ?>
			</ul>

		</div>	

</div>
<?php get_template_part('includes/part-custom-bottom'); ?>

<?php get_template_part( 'includes/part-newsletter' ); ?>

<?php get_footer(); ?>
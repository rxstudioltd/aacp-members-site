<?php
/*
Template Name: Documents page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.white-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg);
		height:280px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>

</div>	<!-- Grey bg end -->
<div class="search-area-holder">
	<div class="container m-bot-35 clearfix">
		<?php dynamic_sidebar('Search Area'); ?>
	</div>
</div>
<div class="members-menu-holder">
	<div class="container m-bot-35 clearfix">
		<?php 
			echo do_shortcode( '[su_members class="members-menu-alert"][su_menu name="Members Menu" class="sf-menu clearfix"][/su_members]' );
		?>
	</div>
</div>

<div class="container m-bot-35 clearfix">

		<div class="sixteen columns main-pages">
			<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?> 
			<h2><?php the_title(); ?></h2>
			<?php the_content(); ?>
			<?php endwhile; ?>
			<?php endif; ?>

			<div style="display:none;">
				<?php echo do_shortcode('[su_accordion class=""][su_spoiler]Content[/su_spoiler][/su_accordion]'); ?>
			</div>

			<?php
				$terms = get_terms('document-type', array( 'parent' => 0 ));
				if ( !empty( $terms ) && !is_wp_error( $terms ) ){
					echo "<div class='su-accordion'>";
					foreach ( $terms as $term ) {
					echo "<div class='su-spoiler su-spoiler-style-fancy su-spoiler-icon-plus su-spoiler-closed'><div class='su-spoiler-title'><span class='su-spoiler-icon'></span>" . $term->name . "</div><div class='su-spoiler-content su-clearfix'>";
					$parent_cat = $term->term_id;
					$taxonomy = 'document-type';
					$cat_children = get_term_children( $parent_cat, $taxonomy );

					if ($cat_children) {
					    foreach($cat_children as $category) {
					    $termed = get_term_by( 'id', $category, $taxonomy );
					    $args=array(
					      'document-type' =>  $termed->name,
					      'post_type' => 'documents',
					      'post_status' => 'publish',
					      'posts_per_page' => -1,
					      'ignore_sticky_posts'=> 1
					    );
					    $my_query = null;
					    $my_query = new WP_Query($args);
					    if( $my_query->have_posts() ) {
					      echo '<div class="documents-inner"><h3>'.$termed->name.'</h3><hr>';
					      while ($my_query->have_posts()) : $my_query->the_post(); ?>
					        <div class="documents-item">
					        	<div class="documents-item-text">
					        		<h5><?php the_title(); ?></h5>
					        		<?php the_content(); ?>
					        	</div>
					        	<div class="documents-item-meta">
					        		<a class="blue-button" href="<?php the_field('document_upload'); ?>">DOWNLOAD</a>
					        	</div>
					        </div>
					        <?php endwhile;
					        echo '</div>';
					    }
					  }
					}
					wp_reset_query();  // Restore global post data stomped by the_post().
					echo "</div></div>";
				}
					echo "</div>";
				}
			?>
			
		</div>


		
		</div>	
<?php get_template_part( 'includes/part-latest-news' ); ?>
</div>


<?php get_template_part('includes/part-custom-bottom'); ?>

<?php get_template_part( 'includes/part-newsletter' ); ?>
	
<?php get_footer(); ?>
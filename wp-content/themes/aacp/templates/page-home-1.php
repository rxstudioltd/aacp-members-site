<?php
/*
Template Name: Homepage 1
*/
get_header();
$homepage = 'Homepage 1';
?>
<?php
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<?php get_template_part('includes/part-custom-top'); ?>
</div>	<!-- Grey bg end -->
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
<div class="bannerarea">
	<div class="container clearfix">
		<div class="slider-1 clearfix">
			<div class="flex-container">
				<div class="flexslider">
					<ul class="slides">
						<li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2; background: url(http://aacpmain.pharma-mix.com/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg) 50% 0px no-repeat;" class="flex-active-slide">
							<div class="container">
								<div class="sixteen columns contain">
									<p data-bottomtext="22%" style="bottom: 22%; left: 0px; opacity: 1;"></p>
								</div>
							</div>
						</li>
					</ul>	
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container clearfix">
	<div class="eleven columns">
		<div class="hometext"><?php the_content(); ?></div>
	</div>
	<div class="five columns mainpartner-holder">
		<img src="/wp-content/themes/aacp/images/mainpartner.png">
	</div>
</div>
<?php endwhile; endif; ?>	

<div class="quickboxes-holder">
	<?php get_template_part('includes/part-custom-bottom'); ?>
</div>
<div class="publicinfoscroller">
</div>
<div class="latestnewsarea">
	<?php get_template_part( 'includes/part-latest-news' ); ?>
</div>


<?php get_template_part( 'includes/part-newsletter' ); ?>

<?php get_footer(); ?>
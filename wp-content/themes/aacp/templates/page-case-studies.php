<?php
/*
Template Name: Case Studies page
*/
get_header();
?>
<?php
$page_title = get_post_meta($post->ID, 'iweb_page_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.white-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg);
		height:280px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>

</div>	<!-- Grey bg end -->
<div class="search-area-holder">
	<div class="container m-bot-35 clearfix">
		<?php dynamic_sidebar('Search Area'); ?>
	</div>
</div>
<div class="members-menu-holder">
	<div class="container m-bot-35 clearfix">
		<?php 
			echo do_shortcode( '[su_members class="members-menu-alert"][su_menu name="Members Menu" class="sf-menu clearfix"][/su_members]' );
		?>
	</div>
</div>
<div class="container m-bot-35 clearfix">
		<div class="sixteen columns  main-pages">
			<h1><?php the_title(); ?></h1>
			<?php
				$args = array(
					'post_type' =>'Case Studies',
					'post_per_page' => -1,
				);
				$query = new WP_Query($args);
				while ($query->have_posts()) : $query->the_post();
			?>
				<div class="case-study-holder"> 
					<div class="case-study-text">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
					<div class="case-study-info">
						<?php if( get_field('date_created') ):?>
							<div class="info-item">Date created:<br><?php the_field('date_created'); ?></div>
						<?php endif; ?>
						<?php if( get_field('file_size') ):?>
							<div class="info-item">File size:<br><?php the_field('file_size'); ?></div>
						<?php endif; ?>
						<?php if( get_field('file_type') ):?>
							<div class="info-item">File type:<br><?php the_field('file_type'); ?></div>
						<?php endif; ?>
						<?php if( get_field('owner') ):?>
							<div class="info-item">Owner:<br><?php the_field('owner'); ?></div>
						<?php endif; ?>
						<?php if( get_field('attach_document') ):?>
							<div class="info-item-last"><a class="blue-button" href="<?php the_field('attach_document'); ?>">DOWNLOAD</a></div>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>

		</div>	
</div>
<?php get_template_part('includes/part-custom-bottom'); ?>
<?php if($mb_portfolio == 'Enable') { ?>
	<?php get_template_part( 'includes/part-portfolio' ); ?>
<?php } ?>

<?php get_template_part( 'includes/part-newsletter' ); ?>

<?php if($mb_clients == 'Enable') { ?>
	<?php get_template_part( 'includes/part-clients' ); ?>
<?php } ?>	
<?php get_footer(); ?>
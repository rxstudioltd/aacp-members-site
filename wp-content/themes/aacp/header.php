<?php
/**
 * @package WordPress
 */
if (empty($feed_url)) { $feed_url = get_bloginfo('rss2_url'); }
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- Favicons
	================================================== -->
<?php
	$favicon = ''; $favicon = iwebtheme_smof_data('favicon');
	if (empty($favicon)) { ?>
	<link link rel="shortcut icon" href="<?php echo get_template_directory_uri().'/images/favicon.png' ?>" />
<?php }	else { ?>
	<link rel="icon" type="image/png" href="<?php echo $favicon ?>" />
<?php } ?>	
<!-- wp head -->
<?php 
wp_head(); 
?>

</head>
<body <?php body_class( 'body' ); ?>>

<div id="wrap" class="boxed">
<div class="white-bg"> <!-- Grey bg  -->	

	<!--[if lte IE 7]>
	<div id="ie-container">
		<div id="ie-cont-close">
			<a href='#' onclick='javascript&#058;this.parentNode.parentNode.style.display="none"; return false;'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-close.jpg' style='border: none;' alt='Close'></a>
		</div>
		<div id="ie-cont-content" >
			<div id="ie-cont-warning">
				<img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning.jpg' alt='Warning!'>
			</div>
			<div id="ie-cont-text" >
				<div id="ie-text-bold">
					You are using an outdated browser
				</div>
				<div id="ie-text">
					For a better experience using this site, please upgrade to a modern web browser.
				</div>
			</div>
			<div id="ie-cont-brows" >
				<a href='http://www.firefox.com' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-firefox.jpg' alt='Download Firefox'></a>
				<a href='http://www.opera.com/download/' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-opera.jpg' alt='Download Opera'></a>
				<a href='http://www.apple.com/safari/download/' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-safari.jpg' alt='Download Safari'></a>
				<a href='http://www.google.com/chrome' target='_blank'><img src='<?php echo get_template_directory_uri('template_directory'); ?>/images/ie-warning-chrome.jpg' alt='Download Google Chrome'></a>
			</div>
		</div>
	</div>
	<![endif]-->
		<header id="header">
			<div class="container clearfix">
				<div class="sixteen columns header-position">

					<div class="header-container clearfix">
						
						<div class="header-logo-container ">
							<div class="logo-container">

							<a href="<?php echo home_url(); ?>/" title="<?php bloginfo( 'name' ); ?>" class="logo">
								<img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/logo.png" alt="" />
							</a>
		
							</div>
						</div>

						<?php global $current_user;
      						get_currentuserinfo();
      					?>

						<?php
						if ( is_user_logged_in() ) {
							echo '<div class="members-welcome-box">';
							echo '<div class="members-welcome-box-icon"><img src="'?><?php echo get_template_directory_uri('template_directory'); ?><?php echo '/images/your-membership-icon-small.png" /></div>';
							echo '<div class="members-welcome-box-title"><h6>Welcome</h6></div>';
							echo '<div class="members-welcome-box-name"><h4>' . $current_user->user_firstname . ' ';
      						echo '' . $current_user->user_lastname . '</h4></div>';
      						echo '<div class="members-welcome-box-logout">';
      						wp_loginout();
      						echo '</div></div>';
						} else {
							echo '<div class="loginboxholder">';
								wp_login_form();
							echo '</div>';
						}
						?>
					
					</div>
				</div>
			</div>

		</header>


		<div class="header-menu-container">	
			<div class="container clearfix">
				<div class="sixteen columns">
					<nav id="main-nav">
						<?php wp_nav_menu(array('menu_class' => 'sf-menu clearfix', 'menu' => 'Primary Navigation' )); ?>
					</nav>
				</div>
			</div>
		</div>




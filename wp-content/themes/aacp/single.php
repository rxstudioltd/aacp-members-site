<?php
/**
 * @package WordPress
 */
$sidebar_pos = iwebtheme_smof_data('sidebar_pos');
?>
<?php get_header(); ?>
<?php
$post_title = get_post_meta($post->ID, 'iweb_post_title', TRUE); 
$mb_portfolio = get_post_meta($post->ID, 'iweb_page_portfolio', TRUE);
$mb_signup = get_post_meta($post->ID, 'iweb_page_signup', TRUE); 
$mb_clients = get_post_meta($post->ID, 'iweb_page_clients', TRUE); 
?>
<style>
	.white-bg {
		background-image:url(/wp-content/uploads/2014/02/slider-bg-aacp-3.jpg);
		height:280px;
		background-color: transparent !important;
		background-position: 50% 80%;
	}	
</style>

</div>	<!-- Grey bg end -->
<div class="members-menu-holder">
	<div class="container m-bot-35 clearfix">
		<?php 
			echo do_shortcode( '[su_members class="members-menu-alert"][su_menu name="Members Menu" class="sf-menu clearfix"][/su_members]' );
		?>
	</div>
</div>
<div class="container clearfix">

	<div class="sixteen columns m-bot-25">
			<?php get_template_part( 'loop' , 'content');?>			
	</div>	

</div>     

<?php if($mb_signup != 'Disable') { ?>
	<?php get_template_part( 'includes/part-newsletter' ); ?>
<?php } ?>

<?php get_footer(); ?>
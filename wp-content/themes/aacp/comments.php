<?php
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.','iwebtheme'); ?></p>
	<?php
		return;
	}
?>


<?php if ( comments_open() ) : ?>

	<div class="caption-container-main m-bot-30">
    <div class="caption-text-container"><?php comments_number(__('0 COMMENTS', 'iwebtheme'), __('1 COMMENT', 'iwebtheme'), __('% COMMENTS', 'iwebtheme') );?></div>
	<div class="content-container-white caption-bg "></div>
	</div>

<?php if ( have_comments() ) : ?>
	<div class="comment-list">
		<ol>
			<?php wp_list_comments( array( 'callback' => 'custom_comments', 'style' => 'ol', 'avatar_size' => '80' ) ); ?>
		</ol><!-- .commentlist -->
	</div>


<div class="comment-nav">
	<div class="alignleft"><?php previous_comments_link() ?></div>
	<div class="alignright"><?php next_comments_link() ?></div>
</div>

<?php endif; ?>
<?php else :
// comments are closed ?>
<?php endif; ?>


<?php if ( comments_open() ) : ?>


<div id="respond" class="reply_form_container">
			<div class="caption-container-main m-bot-30">
				<div class="caption-text-container"><?php _e('<span class="bold">LEAVE</span> A COMMENT','iwebtheme') ?></div>
				<div class="content-container-white caption-bg "></div>
			</div>

<div class="cancel-comment-reply">
<?php cancel_comment_reply_link(); ?>
</div>
<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p><?php _e('You must be','iwebtheme'); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in','iwebtheme'); ?></a><?php _e(' to post a comment.','iwebtheme'); ?></p>

<?php else : ?>
<div class="leave-comment-container">
<form id="comment-form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" class="clearfix" method="post">

<?php if ( is_user_logged_in() ) : ?>

<p><?php _e('Logged in as','iwebtheme'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account','iwebtheme'); ?>"><?php _e('Log out','iwebtheme'); ?> &raquo;</a></p>

<?php else : ?>
					<fieldset class="field-1-2-comment left">
						<label><?php echo __('Name','iwebtheme'); ?></label>
						<input type="text" name="author" id="Myname" onblur="if(this.value=='')this.value='Your name...';" onfocus="if(this.value=='Your name...')this.value='';" value="Your name..." class="text requiredField m-bot-20" >
					</fieldset >
					<fieldset class="field-1-2-comment left">
						<label><?php echo __('Email','iwebtheme'); ?></label>	
						<input type="text" name="email" id="myemail" onblur="if(this.value=='')this.value='Your email...';" onfocus="if(this.value=='Your email...')this.value='';" value="Your email..."  class="text requiredField email m-bot-20" >
					</fieldset>




<?php endif; ?>
					<fieldset class="field-1-1-comment">
						<label><?php echo __('Message','iwebtheme'); ?></label>
						<textarea name="comment" id="comment" rows="5" cols="30" class="text requiredField" onblur="if(this.value=='')this.value='Your message...';" onfocus="if(this.value=='Your message...')this.value='';"   ><?php echo __('Your message','iwebtheme'); ?>...</textarea>
					</fieldset>
					<fieldset class="right">
						<input name="Mysubmitted" id="Mysubmitted" value="<?php echo __('SEND','iwebtheme'); ?>" class="button medium" type="submit" >
					</fieldset>

<?php comment_id_fields(); ?>
<?php do_action('comment_form', $post->ID); ?>

</form>
</div>
<?php endif;
// registration required and not logged in ?>

</div>

<?php else :
comment_form();
// comments are closed ?>
<?php endif;
// delete me and the sky will fall on your brain ?>
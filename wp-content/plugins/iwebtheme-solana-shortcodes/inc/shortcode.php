<?php
/* container content */

function shortcode_container( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'clear' => '', 'marginbottom' => '', 'margintop' => '', 'padding' => ''), $atts
	));
	
	$pad = '';
	if($padding == '0') {
		$pad = '';
	} else if($padding == 'top 15px') {
		$pad = 'pad-t-15 ';
	} else if($padding == 'top-bottom 30px') {
		$pad = 'pad-t-b-30';
	}
	
	
	if($clear == 'clearfix') {
		$html = '<div class="container clearfix '.$marginbottom.' '.$margintop.' '.$pad.'">' . do_shortcode($content) . '</div>';
	}
	if($clear == 'no') {
		$html = '<div class="container '.$marginbottom.' '.$margintop.' '.$pad.'">' . do_shortcode($content) . '</div>';
	}
	
	return $html;
}
add_shortcode('container', 'shortcode_container');

/* clearfix */

function shortcode_clearfix( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => ''), $atts
	));	
	$html = '<div class="clearfix">' . do_shortcode($content) . '</div>';

	return $html;
}
add_shortcode('clearfix', 'shortcode_clearfix');


/* divider */

function shortcode_divider( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => ''), $atts
	));	
	$html = '<hr class="'.$style.'">';

	return $html;
}
add_shortcode('divider', 'shortcode_divider');

/* ---------------------------------------------- */
/* row */

function shortcode_row( $atts, $content = null ) {
	
	$html = '<div class="row">' . do_shortcode($content) . '</div>';

	return $html;
}
add_shortcode('row', 'shortcode_row');

/* ---------------------------------------------- */

/* column */

function shortcode_column( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'size' => '','type' => '', 'position' => '', 'marginbottom' => ''), $atts
	));
	return '<div class="'.$size.' '.$type.' '.$position.' '.$marginbottom.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('column', 'shortcode_column');

/* -----------------------------------------------------------------


/** Newsletter box **/
function shortcode_newsletter( $atts, $content = null) {
	extract(shortcode_atts(
		array( 'title' => '' , 'buttontext' => '' , 'text' => ''), 
		$atts
	));

	$imgpath = get_template_directory_uri('template_directory');
	$html = '<div class="newsletter-container clearfix">
				<div class="nl-img-container">';
	$html .= '<img src="'.$imgpath.'/images/icon-mail.png" alt="" />';
	$html .= '</div>
				<div class="nl-text-container clearfix">
					<div class="caption">'.$title.'</div>
					<div class="nl-text">'.$text.'</div>
					<div class="nl-form-container">
					<span class="response"></span>
						<form id="mailchimp" class="newsletterform" method="get" action="">
							<input type="text" placeholder="Your email here..." name="email" id="email"><button class="nl-button" type="submit">'.$buttontext.'</button>
						</form>
					</div>	
				</div>
			</div>';

	return $html;
}
add_shortcode('newsletter', 'shortcode_newsletter');

/* -----------------------------------------------------------------


/* block column */

function shortcode_blockcolumn( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'size' => '','marginbottom' => ''), $atts
	));
	return '<div class="'.$size.' '.$marginbottom.'">' . do_shortcode($content) . '</div>';
}
add_shortcode('blockcolumn', 'shortcode_blockcolumn');

/* -----------------------------------------------------------------

/** Block **/
function shortcode_block( $atts, $content = null) {
	extract(shortcode_atts(
		array( 'version' => '' , 'bg' => '' , 'title' => '' , 'icon' => '' , 'btntxt' => '' , 'url' => '', 'content' => '', 'target' => ''), 
		$atts
	));
	$boldtitle  = '';
	$boldtitle = explode(' ', $title);
	if (count($boldtitle) > 1 ) {
	$boldtitle[count($boldtitle)-1] = '<span class="bold">'.($boldtitle[count($boldtitle)-1]).'</span>';
	$boldtitle = implode(' ', $boldtitle); 
	}  
	
	if($bg == 'bg1') {
		$bgl = '1';
	}
	if($bg == 'bg2') {
		$bgl = '2';
	}
	if($bg == 'bg3') {
		$bgl = '3';
	}
	if($bg == 'bg4') {
		$bgl = '4';
	}
	
	if($version == 'block0white') {
		$html = '<div class="block-0-content-container ca-menu"><div class="block-text">'.$boldtitle.'</div>
				<div class="block-r-m-container"><a class="button medium r-m-plus r-m-full" href="'.$url.'">'.$btntxt.'</a></div></div>';
	}
	
	if($version == 'block0color') {
		$html = '<div class="block-0-content-container block-black-text ca-menu"><div class="block-text">'.$title.'</div>
				<div class="block-r-m-container"><a class="button medium r-m-plus r-m-full" href="'.$url.'">'.$btntxt.'</a></div></div>';
	}

	if($version == 'block1') {
		$html = '<div class="block-'.$bgl.'-content-container ca-menu"><a class=" clearfix" href="'.$url.'" target="'.$target.'">
				<div class="ca-icon"><img src="'.$icon.'" alt="" /></div>
				<div class="ca-content"><h2 class="ca-main">'.$title.'</h2><h3 class="ca-sub">'.$content.'</h3></div>
				</a>
				</div>';
	}

	if($version == 'block2') {
		$html = '<div class="block2-container content-container-white">
				<a class="block2-a clearfix" href="'.$url.'">
				<div class="box2-img-container"><img src="'.$icon.'" alt="" /></div>
				<div class="box2-text-container"><h3 class="long-text">'.$title.'</h3></div>
				</a></div>';
	}
	
	if($version == 'block3') {
		$html = '<div class="block-1-content-container block-black ca-menu">
				<a class="clearfix" href="'.$url.'">
				<div class="ca-icon"><img src="'.$icon.'" alt="" /></div>
				<div class="ca-content"><h2 class="ca-main">'.$title.'</h2><h3 class="ca-sub">'.$content.'</h3></div>
				</a>
				</div>';
	}
	
	if($version == 'block4') {
		$html = '<div class="content-container-black ca-menu">
				<a class="clearfix" href="'.$url.'">
				<div class="ca-icon"><img src="'.$icon.'" alt="" /></div>
				<div class="ca-content"><h2 class="ca-main">'.$title.'</h2><h3 class="ca-sub">'.$content.'</h3></div>
				</a></div>';
	}

	if($version == 'block5') {
		$html = '<div class="block3-container content-container-white-no-shadow m-bot-20">
				<div class="block3-a clearfix" >
				<div class="box3-img-container"><img src="'.$icon.'" alt="" /></div>
				<div class="box3-text-container clearfix"><h3 class="long-text">'.$title.'</h3></div>
				<div class="box3-description-container"><p class="box3-description">'.$content.'</p></div>
				<a class="right button medium" href="'.$url.'">'.$btntxt.'</a>
				</div>
				</div>';
	}
	
	return $html;
}
add_shortcode('block', 'shortcode_block');

/* -----------------------------------------------------------------

/** Box **/
function shortcode_box( $atts, $content = null) {
	extract(shortcode_atts(
		array( 'icon' => '' , 'title' => '' , 'content' => ''), 
		$atts
	));

	$html = '<article class="box-icon clearfix"><i class="'.$icon.'"></i>
				<h3>'.$title.'</h3><p>'.$content.'</p>
			</article>';
	
	return $html;
}
add_shortcode('box', 'shortcode_box');
/* -----------------------------------------------------------------




/* title-block */
function shortcode_titleblock( $atts, $txt = null ) {
	extract(shortcode_atts(
		array( 'size' => ''), $atts
	));
	
	return '<'.$size.' class="title-block">' .$txt. '</'.$size.'>';
}
add_shortcode('titleblock', 'shortcode_titleblock');



/* caption title */
function shortcode_captiontitle( $atts, $txt = null ) {
	
	return '<div class="caption-container-main m-bot-30"><div class="caption-text-container">' .$txt. '</div><div class="content-container-white caption-bg"></div></div>';
}
add_shortcode('captiontitle', 'shortcode_captiontitle');

/* features text */
function shortcode_featurestext( $atts, $txt = null ) {
	
	return '<div class="features-2-text">' .$txt. '</div>';
}
add_shortcode('featurestext', 'shortcode_featurestext');


/* toggle */
function shortcode_toggle( $atts, $content = null ) {
	return '<ul id="toggle-view">' . do_shortcode($content) . '</ul>';
}
add_shortcode('toggle-wrapper', 'shortcode_toggle');

/* toggle title */
function shortcode_toggle_title( $atts, $content = null ) {
	return '<li><h3 class="ui-accordion-header"><span class="link"></span>' . $content . '</h3>';
}
add_shortcode('toggle-title', 'shortcode_toggle_title');

/* toggle body */
function shortcode_toggle_body( $atts, $content = null ) {
	return '<div class="panel"><p>' . do_shortcode($content) . '</p></div></li>';
}
add_shortcode('toggle-body', 'shortcode_toggle_body');

/* ----------------------------------------------------- */

/* accordion */
function shortcode_accordion( $atts, $content = null ) {
	return '<div class="accordion">' . do_shortcode($content) . '</div>';
}
add_shortcode('accordion-wrapper', 'shortcode_accordion');

/* accordion title */
function shortcode_accor_title( $atts, $content = null ) {
	return '<h3><a href="#">' . $content . '</a></h3>';
}
add_shortcode('accordion-title', 'shortcode_accor_title');

/* accordion body */
function shortcode_accor_body( $atts, $content = null ) {
	return '<div><p>' . do_shortcode($content) . '</p></div>';
}
add_shortcode('accordion-body', 'shortcode_accor_body');

/* ------------------------------------------------------------- */

/** tab **/

/** tab wrapper **/
function shortcode_tab_wrapper( $atts, $content = null ) {
	return '<div class="tabs-widget">' . do_shortcode($content) . '</div>';
}
add_shortcode('tab-wrapper', 'shortcode_tab_wrapper');

/** tab body **/
function shortcode_tab_body( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'class' => '','id' => ''), $atts
	));
	return '<div id="tab-'.$id.'" class="tab-content"><ul class="tab-post-container text"><li><p>' . do_shortcode($content) . '</p></li></ul></div>';
}
add_shortcode('tab-body', 'shortcode_tab_body');

function shortcode_tab_body_wrapper( $atts, $content = null ) {
	return '<div class="tabs-container">' . do_shortcode($content) . '</div>';
}
add_shortcode('tab-body-wrapper', 'shortcode_tab_body_wrapper');

/** tab title wrapper **/
function shortcode_tab_title_wrapper( $atts, $content = null ) {
	return '<ul class="tabs-nav">' . do_shortcode($content) . '</ul>';
}
add_shortcode('tab-title-wrapper', 'shortcode_tab_title_wrapper');

/** tab title **/
function shortcode_tab_title( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'class' => '','type' => '','id' => ''), $atts
	));
	
	if($type == 'active') {
		return '<li class="active"><a href="#tab-'.$id.'">' . $content . '</a></li>';
	} else {
		return '<li><a href="#tab-'.$id.'">' . $content . '</a></li>';
	}
	
	
}
add_shortcode('tab-title', 'shortcode_tab_title');

/* ----------------------------------------------------------------------- */

/* button */
function shortcode_button( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'url' => '', 'style' => '', 'icon' => '', 'iconposition' => ''), $atts
	));
	

	$html = '';
	if($icon != ''){
		$html .= '<a href="'.$url.'" class="button '.$style.'"><i class="'.$icon.'"></i>' . do_shortcode($content) . '</a>';	

	} else if($icon == '') {
		$html .= '<a href="'.$url.'" class="button '.$style.'">' . do_shortcode($content) . '</a>';	
	}
	
	return $html;
}
add_shortcode('button', 'shortcode_button');



/* alert */
function shortcode_alert( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => '', 'title' => '', 'content' => '' ), $atts
	));

	
	return '<div class="styled-box iconed-box '.$style.'"><strong>'.$title.'</strong> - '.$content.'</div>';
}
add_shortcode('alert', 'shortcode_alert');

/* dropcaps */
function shortcode_dropcaps( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => '','letter' => ''), $atts
	));
	return '<p><span class="'.$style.'">' .$letter. '</span>' .$content. '</p>';
}
add_shortcode('dropcaps', 'shortcode_dropcaps');


/* skillbars */
function shortcode_skills( $atts, $content = null ) {
	return '<div id="skill-bars">' . do_shortcode($content) . '</div>';
}
add_shortcode('skills', 'shortcode_skills');

/* skillbar skill */
function shortcode_skill( $atts, $content = null ) {
	return '<div class="skill-bar">' . do_shortcode($content) . '</div>';
}
add_shortcode('skill', 'shortcode_skill');

/* skillbar name */
function shortcode_skill_name( $atts, $content = null ) {
	return '<span class="skill-title">' . $content . '</span>';
}
add_shortcode('skill-name', 'shortcode_skill_name');

/* skillbar percentage */
function shortcode_skill_percentage( $atts, $content = null ) {
	return '<div class="skill-bar-content" data-percentage="'.do_shortcode($content).'"></div>';
}
add_shortcode('skill-percentage', 'shortcode_skill_percentage');


/* Team */
function shortcode_team( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'role' => '', 'name' => '', 'img' => '', 'facebook' => '', 'twitter' => '', 'linkedin' => '', 'skype' => ''), $atts
	));
	
	$html ='<div class="content-container-white m-bot-35 clearfix">'; 
	$html .='<img src="'.$img.'" alt="'.$name.'" />';
    $html .='<ul class="social-links clearfix">';
	if($facebook != '') {
	$html .='<li><a class="facebook-link" target="_blank" title="facebook" href="'.$facebook.'"></a></li>';
	}
	if($twitter != '') {
	$html .='<li><a class="twitter-link" target="_blank" title="twitter" href="https://twitter.com/'.$twitter.'"></a></li>';
	}
	if($linkedin != '') {
	$html .='<li><a class="linkedin-link" target="_blank" title="linkedin" href="https://linkedin.com/'.$linkedin.'"></a></li>';
	}
	$html .='</ul>';
    $html .='<div class="lw-item-caption-container">';  
	$html .='<div class="team-name">';	
    $html .='<h5>'.$name.'</h5>';
	$html .='<span>'.$role.'</span>';
	$html .='</div>';
	$html .='</div>';
	$html .='</div>';
	
	return $html;
}
add_shortcode('team', 'shortcode_team');
/* ------------------------------------------------------------ */


/** latest post slider **/
function shortcode_latestposts( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'count' => ''), 
		$atts
	));
	
		global $post;
	
        $type = 'post';
        $args=array(
            'post_type' => $type,
			'showposts' => $count
          );
         query_posts($args);	
		$return_string = '
		<div class="container clearfix m-top-60">
		<div class="four columns carousel-intro m-bot-33">
			<div class="caption-container m-bot-20">
								<div class="title-block-text">' . do_shortcode($content) . '</div>
								<div class="carousel-navi jcarousel-scroll">
									<div class="jcarousel-prev"></div>
									<div class="jcarousel-next"></div>
								</div>
							</div>
							</div>';		 

	   if (have_posts()) :
	   $return_string .= '<div class="jcarousel latest-posts-jc m-bot-50"><ul class="clearfix">';
		
		while (have_posts()) : the_post();
		$postformat = get_post_format();	
		$comment = get_comments_number( $post->ID );
		$comm = '';
			if($comment < 2) {
				$comm = 'Comment';
			} else if($comment > 1) {
				$comm = 'Comments';
			}
		$day=get_the_time('d M Y');
		$title=get_the_title();
		$title = wp_trim_words($title,$num_words =3);
		$title=explode(' ',$title);
		$title[0]='<span class="bold">'.$title[0].'</span>';
		$title=implode(' ',$title);
		$permalink=get_permalink();
		$trimcontent = get_the_content();
		$shortexcerpt = wp_trim_words($trimcontent,$num_words = 18);	
		$image = '';		
		$feathumb = '';
		  
			 $return_string .= '<li class="four columns">';

				if(has_post_thumbnail())  { 
					$featimg = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
					$feathumb = $featimg[0];
					$image = aq_resize($feathumb,400,200, true);
				} 			
				$return_string .= '<div class="hover-item"><div class="view view-first">';
				if($image != '')  {
					$return_string .= '<img src="'.$image.'" alt="" />';	
				}
				$return_string .=   '<div class="mask"></div><div class="abs">
				<a class="lightbox zoom info" href="'.$feathumb.'"></a><a class="link info" href="'.$permalink.'"></a>
								</div>						
								</div>
								<div class="lp-item-caption-container">
									<a class="a-invert" href="'.$permalink.'" >'.$title.'</a>
									<div class="lp-item-container-border clearfix">
										<div class="lp-item-info-container">
											'.$day.' | '.$comment.' '.$comm.'
										</div>
									</div>
								</div>
								</div>';
				
			$return_string .= '<div class="lp-item-text-container">'.$shortexcerpt.'</div><div class="lp-r-m-container">
							<a href="'.$permalink.'" class="r-m-plus-small">READ MORE</a>
						</div>';

		  endwhile;
		  
		$return_string .= '</ul></div></div>';
	   endif;
	   wp_reset_query();
	   return $return_string;
}
add_shortcode('latestposts', 'shortcode_latestposts');
/* -------------------------------------------------------------------------------------------------------

/** latest work slider **/
function shortcode_latestworks( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'count' => ''), 
		$atts
	));
	
		global $post;

        $type = 'portfolio';
        $args=array(
            'post_type' => $type,
			'showposts' => $count
          );
         query_posts($args);	
		$return_string = '<div class="container clearfix m-top-60">
							<div class="four columns carousel-intro m-bot-33">
							<div class="caption-container m-bot-20">
								<div class="title-block-text">' . do_shortcode($content) . '</div>
								<div class="carousel-navi jcarousel-scroll">
									<div class="jcarousel-prev"></div>
									<div class="jcarousel-next"></div>
								</div>
							</div>
							</div>';		 

	   if (have_posts()) :
	   $return_string .= '<div class="jcarousel latest-work-jc m-bot-30" ><ul class="clearfix">';
		
		while (have_posts()) : the_post();
		
			$permalink=get_permalink();
			$title=get_the_title();
			$title = wp_trim_words($title,$num_words =3);
			$title=explode(' ',$title);
			$title[0]='<span class="bold">'.$title[0].'</span>';
			$title=implode(' ',$title);
			
			if (has_post_thumbnail()) {					
						$thumb = get_post_thumbnail_id();
						$thumb_w = '460';
						$thumb_h = '272';
						$image_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
						$image_url = $image_src [0];
						$attachment_url = wp_get_attachment_url($thumb, 'full');
						$image = aq_resize($attachment_url, $thumb_w, $thumb_h, true);							
			}	
			
			$terms = get_the_terms( $post->ID, 'portfolio_categories' );
				foreach ( $terms as $term ) {
					$cats[0] = $term->name;
					$catname = join($cats);		
					$catname = preg_replace('/\s/', '', $catname);											
				}
		  
			$return_string .= '<li class="four columns">';

			$return_string .= '<div class="hover-item">';
			$return_string .= '<div class="view view-first">';
			$return_string .= '<img src="'.$image.'" alt="" /><div class="mask"></div>	';
			$return_string .= '<div class="abs"><a href="'.$attachment_url.'" class="lightbox zoom info"></a><a href="'.$permalink.'" class="link info"></a>';
			$return_string .= '</div>';
			$return_string .= '</div>';
			$return_string .= '<div class="lw-item-caption-container">';
			$return_string .= '<a class="a-invert" href="'.$permalink.'" ><div class="item-title-main-container clearfix"><div class="item-title-text-container">'.$title.'</div></div></a>
								<div class="item-caption">'.$catname.'</div>';
			$return_string .= '</div>';
			$return_string .= '</div></li>';

		  endwhile;
		  
		$return_string .= '</ul></div></div>';
	   endif;
	   wp_reset_query();
	   return $return_string;
}
add_shortcode('latestworks', 'shortcode_latestworks');
/* -------------------------------------------------------------------------------------------------------

/** testimonial slider **/
function shortcode_testimonialslider( $atts ) {
	extract(shortcode_atts(
		array( 'title' => '','count' => '','size' => ''), 
		$atts
	));
	
		global $post;
	
        $type = 'testimonial';
        $args=array(
            'post_type' => $type,
			'showposts' => $count
          );
         query_posts($args);	
	$return_string = '<div class="caption-container content-container-grey block-testimonials">
								<div class="caption-text-container-test-block">' . $title . '</div>
								<div class="carousel-navi jcarousel-scroll">
									<div class="jcarousel-prev"></div>
									<div class="jcarousel-next"></div>
								</div>
							</div>';		 

	   if (have_posts()) :
	   $return_string .= '<div class="jcarousel testimonials-jc m-bot-35"><ul>';
		  while (have_posts()) : the_post();
			 $return_string .= '<li class="'.$size.'"><div class="testimonial-quote "><div class="quote-content content-container-grey">';
			 $return_string .= '<p>'. get_post_meta($post->ID, 'iweb_testomonial_txt', true) .'</p>';
			 $return_string .= '<p class="right-text"><span class="author-testimonial">- '. get_post_meta($post->ID, 'iweb_testomonial_author', true) .'</span><span class="quote-author-description">, '. get_post_meta($post->ID, 'iweb_testomonial_occu', true) .'</span></p>';
			 $return_string .= '</div></div></li>';

		  endwhile;
		  
		$return_string .= '</ul></div>';
	   endif;
	   wp_reset_query();
	   return $return_string;
}
add_shortcode('testimonialslider', 'shortcode_testimonialslider');
/* -------------------------------------------------------------------------------------------------------


/* pricing background */

function shortcode_pricingbg( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => ''), $atts
	));	
	$html = '<div class="price-table-grey-bg clearfix">' . do_shortcode($content) . '</div>';

	return $html;
}
add_shortcode('pricingbg', 'shortcode_pricingbg');

/* pricing table */
function shortcode_pricingtable( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => '','marginleft' => '','type' => '',), $atts
	));
	
	if(($style == 'style 1') && ($type == 'normal')) {
		$style = 'price-col-gray';
		return '<div class="'.$style.'">' . do_shortcode($content) . '</div>';
	}	
	if(($style == 'style 1') && ($type == 'special'))  {
		$style = 'price-col-main';
		return '<div class="'.$style.'">' . do_shortcode($content) . '</div>';
	}	
	if(($style == 'style 1 last') && ($type == 'normal')) {
		$style = 'price-col-gray table-last';
		return '<div class="'.$style.'">' . do_shortcode($content) . '</div>';
	}
	if($style == 'style 2') {
		$style = 'price-col-gray2';
		return '<div class="'.$style.' '.$marginleft.'"><div>' . do_shortcode($content) . '</div></div>';
	}		
	
}
add_shortcode('pricingtable', 'shortcode_pricingtable');


/** pricing body **/
function shortcode_pricing_heading( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'parent' => '','type' => '','title' => '','price' => '','cent' => '','period' => '','curr' => ''), $atts
	));
	
	$class = '';
	if($type == 'special') {
		$class = 'pt-col-main';
	}
	
	if($parent == 'style 1') {
	return '<div><h1>'.$title.'</h1><div class="price-container"><span class="price">'.$curr.''.$price.'.'.$cent. '</span><span class="cents-cont"><span class="place"> /'.$period.'</span></span></div></div>';
	}
	if($parent == 'style 1 last') {
	return '<div><h1>'.$title.'</h1><div class="price-container"><span class="price">'.$curr.''.$price.'.'.$cent. '</span><span class="cents-cont"><span class="place"> /'.$period.'</span></span></div></div>';
	}
	if($parent == 'style 2') {
	return '<h1 class="'.$class.'">'.$title.'</h1><div class="price-container2"><span class="currency">'.$curr.'</span><span class="price">'.$price.'</span><span class="cents-cont"><span class="cents">'.$cent.'</span><span class="place2">'.$period.'</span></span></div>';
	}	

}
add_shortcode('pricing-heading', 'shortcode_pricing_heading');


/** pricing title wrapper **/
function shortcode_pricing_features( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => ''), $atts
	));
	if($style == 'border') {
		$style = 'col-border';
		return '<div class="'.$style.'"><ul>' . do_shortcode($content) . '</ul></div>';
	} else {
		return '<div><ul>' . do_shortcode($content) . '</ul></div>';
	}
	
	
}
add_shortcode('pricing-features', 'shortcode_pricing_features');

/** pricing title **/
function shortcode_pricing_list( $atts, $content = null ) {
	return '<li>' . $content . '</li>';		
}
add_shortcode('pricing-list', 'shortcode_pricing_list');

/** pricing button **/
function shortcode_pricing_button( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'text' => '', 'url' => '', 'parent' => '' , 'type' => ''), $atts
	));
	$class = '';
	if(($parent == 'style 1') && ($type == 'normal')) {
		$class = 'price-button-container';
		return '<div class="'.$class.'"><a class="button medium gray price-button">'.$text.'</a></div>';
	}
	if(($parent == 'style 1 last') && ($type == 'normal')) {
		$class = 'price-button-container';
		return '<div class="'.$class.'"><a class="button medium gray price-button">'.$text.'</a></div>';
	}
	if(($parent == 'style 1') && ($type == 'special')) {
		$class = 'price-button-container-main';
		return '<div class="'.$class.'"><a class="button medium price-button">'.$text.'</a></div>';
	}
	if(($parent == 'style 1 last') && ($type == 'special')) {
		$class = 'price-button-container-main';
		return '<div class="'.$class.'"><a class="button medium price-button">'.$text.'</a></div>';
	}
	if(($parent == 'style 2') && ($type == 'normal')) {
		$class = 'price-button-container';
		return '<div class="'.$class.'"><a class="button medium gray price-button">'.$text.'</a></div>';
	}
	if(($parent == 'style 2') && ($type == 'special')) {
		$class = 'price-button-container';
		return '<div class="'.$class.'"><a class="button medium price-button">'.$text.'</a></div>';
	}
		
}
add_shortcode('pricing-button', 'shortcode_pricing_button');

/* ---------------------------------------------------------------- */

/* callaction */
function shortcode_callaction( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'url' => '', 'title' => '', 'text' => '', 'buttontext' => '' ), $atts
	));
	
	$themepath = get_template_directory_uri('template_directory');
	$title=explode(' ',$title);
	$title[0]='<span class="bold">'.$title[0].'</span>';
	$title=implode(' ',$title);	

	return '<div class="buy-container clearfix"><div class="buy-img-container"><img src="'.$themepath.'/images/icon-buy.png" alt="buy"></div><div class="buy-text-container">
					<div class="buy-text">
						<h2>'.$title.'</h2>
						<span class="buy-now-slogan">'.$text.'</span>
					</div>
				</div>
				<div class="button-buy-container">
					<a href="'.$url.'">'.$buttontext.'</a>
				</div></div>';	

}
add_shortcode('callaction', 'shortcode_callaction');


/* Styled list */
function shortcode_list( $atts, $content = null ) {
	extract(shortcode_atts(
		array( 'style' => ''), $atts
	));
	
	if($style == 'Features') {
		return '<ul class="features-check-list check-icon">' . do_shortcode($content) . '</ul>';
	} else {	
		return '<ul class="styled-list '.$style.'">' . do_shortcode($content) . '</ul>';
	}
}
add_shortcode('list', 'shortcode_list');

/* list */
function shortcode_item( $atts, $content = null ) {
	return '<li>' . do_shortcode($content) . '</li>';
}
add_shortcode('item', 'shortcode_item');
/* --------------------------------------------------------------------------*/


/* break */
function shortcode_break( $atts, $content = null ) {
	
	$html .= '<br>';	

	return $html;
}
add_shortcode('break', 'shortcode_break');
?>
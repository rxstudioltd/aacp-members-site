(function($){
	$('html').themeshortcode({
		id							: 'boxicon',
		title 						: 'Insert box content & icon',			
		image						: 'boxicon.png',
		showWindow					: true,		
		windowWidth					: 800,
		fields						: [ {type : 'text', name : 'Icon class name (font-awesome icons, see http://fortawesome.github.io/Font-Awesome/icons for icon class name)', id : 'txt'} ,
										{type : 'text'  , name : 'Box title'    , id: 'boxtitle'},
										{type : 'text'  , name : 'Box content'    , id: 'boxcontent'}] 
	}, 
	function(ed, url, options) 
	{	
		var iname	= $('#'+options.pluginprefix + 'txt').val();
		var boxtitle 	= $('#'+options.pluginprefix + 'boxtitle').val();
		var boxcontent 	= $('#'+options.pluginprefix + 'boxcontent').val();

		
		var html = '[box icon="'+ iname +'" title="'+ boxtitle +'" content="'+ boxcontent +'"]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
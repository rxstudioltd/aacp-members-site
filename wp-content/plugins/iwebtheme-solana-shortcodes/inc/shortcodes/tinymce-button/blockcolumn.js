(function($){
	$('html').themeshortcode({
		id							: 'blockcolumn',
		title 						: 'Insert block column',			
		image						: 'block-col.png',
		showWindow					: true,
		windowWidth					: 800,
		fields						: [ { type : 'heading' 	   	, name : 'This theme use skeleton 16 columns'},
										
										{type : 'select', name : 'Select block column size ', id : 'select-blockcolumn' , option : ['block 3 columns', 'block 4 columns']},
										{type : 'select'  , name : 'Add additional container margin bottom'     , id: 'select-mbot' , option : ['0','15px', '20px', '25px', '30px', '35px', '50px', '135px']}] 
	}, 
	function(ed, url, options) 
	{	

		var csize 	= $('#'+options.pluginprefix + 'select-blockcolumn').val();
		var mbot 	= $('#'+options.pluginprefix + 'select-mbot').val();

		var sizeclass = '';
		if(csize == "block 3 columns") {
			sizeclass = 'block-3-col';
		} else if(csize == "block 4 columns") {
			sizeclass = 'block-4-col';
		}
	
		var mbotclass = '';
		if(mbot == "0") {
			mbotclass = '0';
		} else if(mbot == "15px") {
			mbotclass = 'm-bot-15';
		} else if(mbot == "20px") {
			mbotclass = 'm-bot-20';
		} else if(mbot == "25px") {
			mbotclass = 'm-bot-25';
		} else if(mbot == "30px") {
			mbotclass = 'm-bot-30';
		} else if(mbot == "35px") {
			mbotclass = 'm-bot-35';
		} else if(mbot == "50px") {
			mbotclass = 'm-bot-50';
		} else if(mbot == "135px") {
			mbotclass = 'm-bot-135';
		}

		
		if(sizeclass == "block-3-col") {
			var html = '[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';		
				html += '<br />[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';	
				html += '<br />[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';			
		}
		if(sizeclass == "block-4-col") {
			var html = '[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';		
				html += '<br />[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';	
				html += '<br />[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';		
				html += '<br />[blockcolumn size="'+ sizeclass +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/blockcolumn]';					
		}


		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
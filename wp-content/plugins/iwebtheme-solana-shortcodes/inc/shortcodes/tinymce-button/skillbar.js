(function($){
	$('html').themeshortcode({
		id							: 'skillbar',
		title 						: 'Skills bar',			
		image						: 'graph.png',
		showWindow					: true,	
		fields						: [ {type : 'inputgrow', name : 'Add graph (just enter skill name only (e.g Web design)'} ] 
	}, 
	function(ed, url, options) 
	{
		var html = '[skills]<br />';
		$(".textgrow").each(function(){
			html += '[skill]<br />';
			html += '[skill-name]' + $(this).val() + '[/skill-name]<br />';
			html += '[skill-percentage]70[/skill-percentage]<br />';
			html += '[/skill]<br />';
		});
		html += '[/skills]\n';
	
		ed.execCommand('mceInsertContent', false, html);

	});
})(jQuery);
(function($){
	$('html').themeshortcode({
		id							: 'toggle',
		title 						: 'Toggle',			
		image						: 'toggle.png',
		showWindow					: true,	
		fields						: [ {type : 'inputgrow', name : 'Add Toggle'} ] 
	}, 
	function(ed, url, options) 
	{
		var html = '[toggle-wrapper]<br />';
		$(".textgrow").each(function(){
			html += '[toggle-title] ' + $(this).val() + ' [/toggle-title]<br />';
			html += '[toggle-body] Put your content here [/toggle-body]<br />';
		});
		html += '[/toggle-wrapper]\n';
	
		ed.execCommand('mceInsertContent', false, html);

	});
})(jQuery);
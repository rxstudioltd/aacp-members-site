(function($){
	$('html').themeshortcode({
			id							: 'latestworks',
			title 						: 'Insert latest works slider (works in fullwidth page)',			
			image						: 'works.png',
			showWindow					: true,	
			fields						: [ {type : 'label',      name : 'Latest works within carousel slider'	, id: 'info'},
											{type : 'text'  , name : 'Latest works numbers in slide (number only)'    , id: 'workscount'}]
		}, 
		function(ed, url, options) 
		{
			var title 			= $('#'+options.pluginprefix + 'workstitle').val();
			var count 			= $('#'+options.pluginprefix + 'workscount').val();

			
			var html = '[latestworks count="'+count+'"]THIS IS THE LIST OF[break] OUR RECENT[break] WORKS[/latestworks]';			
			ed.execCommand('mceInsertContent', false, html);
		});
})(jQuery);
(function($){
	$('html').themeshortcode({
		id							: 'icon',
		title 						: 'Insert font awesome icon',			
		image						: 'iconicon.png',
		showWindow					: true,		
		windowWidth					: 800,
		fields						: [ {type : 'text', name : 'Icon class name (font-awesome icons, see http://fortawesome.github.io/Font-Awesome/icons for icon class name)', id : 'txt'} ,
										{type : 'select', name : 'Select icon size', id : 'select-size' , option : ['default','icon-2x','icon-3x','icon-4x','icon-5x']} ,
										{type : 'select', name : 'Select icon style', id : 'select-style' , option : ['default','light','dark']},
										{type : 'select', name : 'Select icon float', id : 'select-pull' , option : ['none','right','left']} ] 
	}, 
	function(ed, url, options) 
	{	
		var iname	= $('#'+options.pluginprefix + 'txt').val();
		var isize 	= $('#'+options.pluginprefix + 'select-size').val();
		var istyle 	= $('#'+options.pluginprefix + 'select-style').val();
		var ipull 	= $('#'+options.pluginprefix + 'select-pull').val();
		
		var cistyle = '';
		if(istyle == "light") {
			cistyle = 'icon-light';
		} else if(istyle == "dark") {
			cistyle = 'icon-dark';
		} 
		
		var pull = '';
		if(ipull == "right") {
			pull = 'pull-right';
		} else if(ipull == "left") {
			pull = 'pull-left';
		}
		
		
		var html = '[icon name="'+ iname +'" size="'+ isize +'" style="'+ cistyle +'" float="'+ pull +'"]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
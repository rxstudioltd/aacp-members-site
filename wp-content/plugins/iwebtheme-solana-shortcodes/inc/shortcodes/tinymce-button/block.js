(function($){
	$('html').themeshortcode({
		id							: 'block',
		title 						: 'Insert content block',			
		image						: 'block.png',
		showWindow					: true,	
		fields						: [ {type : 'select', name : 'Select block version', id : 'select-version' , option : ['version 0 white','version 0 color','version 1','version 2','version 3','version 4','version 5']} ,
										{type : 'select', name : 'Select background color (darkness level) - Used on block version 1 only ', id : 'select-verbg' , option : ['BG color 1','BG color 2','BG color 3']} ,
										{type : 'text'  , name : 'Block title'     		, id: 'title'}, 
										{type : 'textarea'  , name : 'Block content'     	, id: 'content'}, 
										{type : 'spacer'  , name : 'spacer'}, 
		      						   								
										{type : 'upload', name : 'Upload image icon' 		, id: 'image'},
										{type : 'spacer'  , name : 'spacer'}, 
										{type : 'text'  , name : 'Block link url'     		, id: 'url'},
										{type : 'text'  , name : 'Button link text (used on block version 0 and 5 only)'     , id: 'btntxt'} ]
	}, 
	function(ed, url, options) 
	{
		var title		= $('#'+options.pluginprefix + 'title').val();
		var content		= $('#'+options.pluginprefix + 'content').val();
		var image		= $('#'+options.pluginprefix + 'image').val();
		var version 	= $('#'+options.pluginprefix + 'select-version').val();
		var verbg 	= $('#'+options.pluginprefix + 'select-verbg').val();
		var url		= $('#'+options.pluginprefix + 'url').val();
		var btntxt		= $('#'+options.pluginprefix + 'btntxt').val();
		
		var bg = '';
		if(verbg == "BG color 1") {
			bg = 'bg1';
		} else if(verbg == "BG color 2") {
			bg = 'bg2';
		} if(verbg == "BG color 3") {
			bg = 'bg3';
		}

		var bversion = '';
		if(version == "version 0 white") {
			bversion = 'block0white';
			var html = '[block version="'+ bversion +'" title="'+ title +'" url="'+ url +'" btntxt="'+ btntxt +'"]';
		} else if(version == "version 0 color") {
			bversion = 'block0color';
			var html = '[block version="'+ bversion +'" title="'+ title +'" url="'+ url +'" btntxt="'+ btntxt +'"]';
		} else if(version == "version 1") {
			bversion = 'block1';
			var html = '[block version="'+ bversion +'" bg="'+ bg +'" title="'+ title +'" icon="'+ image +'" url="'+ url +'" content="'+ content +'"]';
		} else if(version == "version 2") {
			bversion = 'block2';
			var html = '[block version="'+ bversion +'" title="'+ title +'" icon="'+ image +'" url="'+ url +'"]';
		} else if(version == "version 3") {
			bversion = 'block3';
			var html = '[block version="'+ bversion +'" title="'+ title +'" icon="'+ image +'" url="'+ url +'" content="'+ content +'"]';
		} else if(version == "version 4") {
			bversion = 'block4';
			var html = '[block version="'+ bversion +'" title="'+ title +'" icon="'+ image +'" url="'+ url +'" content="'+ content +'"]';
		} else if(version == "version 5") {
			bversion = 'block5';
			var html = '[block version="'+ bversion +'" title="'+ title +'" icon="'+ image +'" btntxt="'+ btntxt +'" url="'+ url +'" content="'+ content +'"]';
		} 
		
				
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
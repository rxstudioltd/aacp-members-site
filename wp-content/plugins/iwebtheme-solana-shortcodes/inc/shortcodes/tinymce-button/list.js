(function($){
	$('html').themeshortcode({
		id							: 'list',
		title 						: 'Insert styled list',			
		image						: 'checklist.png',
		showWindow					: true,	
		fields						: [ {type : 'select', name : 'Select list style ', id : 'select-style' , option : ['Features','style-1','style-2','style-3','style-4','style-5','style-6']}] 
	}, 
	function(ed, url, options) 
	{
		var style 	= $('#'+options.pluginprefix + 'select-style').val();
		
		var html = '[list style="'+ style +'"]<br />';
			html += '[item]List item here[/item]<br />';
			html += '[item]List item here[/item]<br />';
			html += '[item]List item here[/item]<br />';
			html += '[/list]';
				
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
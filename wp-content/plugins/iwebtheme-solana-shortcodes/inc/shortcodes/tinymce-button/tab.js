(function($){
	$('html').themeshortcode({
		id							: 'tab',
		title 						: 'Tabbed Content',			
		image						: 'tab_breakoff.png',
		showWindow					: true,	
		fields						: [ {type : 'inputgrow', name : 'Add Tab'} ] 
	}, 
	function(ed, url, options) 
	{		
		var html = '[tab-wrapper]<br />[tab-title-wrapper]<br />';
			var i = 0;
			$(".textgrow").each(function(){
				if(i == 0) {
					html += '[tab-title type="active" id="'+i+'"]' + $(this).val() + '[/tab-title]<br />';
				} else {
					html += '[tab-title id="'+i+'"]' + $(this).val() + '[/tab-title]<br />';
				}
				i++;
			});
			html += '[/tab-title-wrapper]<br />';
		
			html += '[tab-body-wrapper]<br />';
			
			var i = 0;
			$(".textgrow").each(function(){
				if(i == 0) {
					html += '[tab-body id="'+i+'"]' + 'Content of ' + $(this).val() + ' tab' + '[/tab-body]<br />';
				} else {
					html += '[tab-body id="'+i+'"]' + 'Content of ' + $(this).val() + ' tab' + '[/tab-body]<br />';
				}
				i++;
			});
			
			html += '[/tab-body-wrapper][/tab-wrapper]';
			
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
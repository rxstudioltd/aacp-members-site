(function($){
	$('html').themeshortcode({
		id							: 'featurestext',
		title 						: 'Insert features text',			
		image						: 'feat-text.png',
		showWindow					: true,
		fields						: [	{ type : 'text' , name : 'Features text' , id: 'txt'}] 
	}, 
	function(ed, url, options) 
	{
		var txt 	= $('#'+options.pluginprefix + 'txt').val();

		
		var html = '[featurestext]' + txt + '[/featurestext]';		
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
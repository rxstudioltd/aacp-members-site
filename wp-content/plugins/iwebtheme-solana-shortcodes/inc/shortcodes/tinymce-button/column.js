(function($){
	$('html').themeshortcode({
		id							: 'column',
		title 						: 'Insert column',			
		image						: 'column.png',
		showWindow					: true,
		windowWidth					: 800,
		fields						: [ { type : 'heading' 	   	, name : 'This theme use skeleton 16 columns'},
										
										{type : 'select', name : 'Select column size ', id : 'select-column' , option : ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'one-third', 'two-thirds', 'in-one-third','in-two-thirds','in-sixteen','in-eight','in-four','in-three-four']} ,
										{type : 'select'  , name : 'Set column type'     , id: 'select-coltype' , option : ['columns', 'in-columns','column', 'in-column']},
										{type : 'select'  , name : 'Add additional container margin bottom'     , id: 'select-mbot' , option : ['0','15px', '20px', '25px', '30px', '35px', '50px', '135px']},
										{type : 'select'  , name : 'Select column position'     , id: 'select-position' , option : ['First', 'Normal','Last']}] 
	}, 
	function(ed, url, options) 
	{	

		var csize 	= $('#'+options.pluginprefix + 'select-column').val();
		var cpos 	= $('#'+options.pluginprefix + 'select-position').val();
		var ctype 	= $('#'+options.pluginprefix + 'select-coltype').val();
		var conbg 	= $('#'+options.pluginprefix + 'select-bg').val();
		var mbot 	= $('#'+options.pluginprefix + 'select-mbot').val();
		var cpad 	= $('#'+options.pluginprefix + 'select-pad').val();
		

	
		var mbotclass = '';
		if(mbot == "0") {
			mbotclass = '0';
		} else if(mbot == "15px") {
			mbotclass = 'm-bot-15';
		} else if(mbot == "20px") {
			mbotclass = 'm-bot-20';
		} else if(mbot == "25px") {
			mbotclass = 'm-bot-25';
		} else if(mbot == "30px") {
			mbotclass = 'm-bot-30';
		} else if(mbot == "35px") {
			mbotclass = 'm-bot-35';
		} else if(mbot == "50px") {
			mbotclass = 'm-bot-50';
		} else if(mbot == "135px") {
			mbotclass = 'm-bot-135';
		}
		
		var classbg = '';
		if(conbg == "none") {
			classbg = 'nobg';
		} else if(conbg == "white background for container") {
			classbg = 'content-container-white';
		} 
		
		var classpad = '';
		if(cpad == "none") {
			classpad = 'nopadding';
		} else if(cpad == "padding left right 5px") {
			classpad = 'pad-l-r-5';
		} else if(cpad == "padding left right 15px") {
			classpad = 'padding-l-r-15';
		} else if(cpad == "padding all 15px") {
			classpad = 'padding-all-15';
		}
		
		var classpos = '';
		if(cpos == "First") {
			classpos = 'alpha';
		} else if(cpos == "Normal") {
			classpos = '';
		} else if(cpos == "Last") {
			classpos = 'omega';
		} 

			var html = '[column size="'+ csize +'" type="'+ ctype +'" position="'+ classpos +'" marginbottom="'+ mbotclass +'"]<br />';
				html += 'your content here';
				html += '<br />[/column]';

		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
(function($){
	$('html').themeshortcode({
		id							: 'pricingbox',
		title 						: 'Insert pricing box (one by one package table)',			
		image						: 'pricingbox.png',
		showWindow					: true,	
		fields						: [ { type : 'select' , name : 'Style ("style 1 last" must selected for last/right style 1 table in a row)', id: 'select-style', option : ['style 1', 'style 1 last', 'style 2']},										 
										{ type : 'select' , name : 'Table column type'	, id: 'select-type', option : ['normal', 'special']},
										{type : 'select' , name : 'left margin (optional - select 0px for first column table on style 2 pricing table )', id: 'select-marginleft', option : ['no set','0px']},
										{type : 'text'  , name : 'Package title'     , id: 'packtitle'},
										{type : 'text'  , name : 'Currency'     , id: 'currency'},
										{type : 'text'  , name : 'Price (round, no comma, number only e.g 10)'     , id: 'price'},
										{type : 'text'  , name : 'Cents (number only eg 99)'     , id: 'cent'},
										{type : 'text'  , name : 'Pricing period (e.g monthly)'     , id: 'period'},
										{type : 'text'  , name : 'Button Url'     , id: 'btnurl'},
										{type : 'text'  , name : 'Button Text'    , id: 'btntxt'}] 
	}, 
	function(ed, url, options) 
	{
		var style 	= $('#'+options.pluginprefix + 'select-style').val();
		var type 	= $('#'+options.pluginprefix + 'select-type').val();
		var marginleft 	= $('#'+options.pluginprefix + 'select-marginleft').val();
		var packtitle = $('#'+options.pluginprefix + 'packtitle').val();
		var currency = $('#'+options.pluginprefix + 'currency').val();	
		var price = $('#'+options.pluginprefix + 'price').val();	
		var cent = $('#'+options.pluginprefix + 'cent').val();	
		var period = $('#'+options.pluginprefix + 'period').val();	
		var url = $('#'+options.pluginprefix + 'btnurl').val();
		var btntxt = $('#'+options.pluginprefix + 'btntxt').val();
		
		var cmarginleft = '';
		if(marginleft == "0px") {
			cmarginleft = 'm-left-0';
		} else if(marginleft == "no set") {
			cmarginleft = '';
		}

		
		var html = '[pricingtable style="'+style+'" type="'+type+'" marginleft="'+cmarginleft+'"]<br />';

			if(style == "style 2" && type == "special") {
			html += '[pricing-heading parent="'+style+'" type="special" title="'+packtitle+'" curr="'+currency+'" price="'+price+'" cent="'+cent+'" period="'+period+'"]<br />';
			} else {
			html += '[pricing-heading parent="'+style+'" title="'+packtitle+'" curr="'+currency+'" price="'+price+'" cent="'+cent+'" period="'+period+'"]<br />';			
			}
			
			if(style == "style 1") {
			html += '[pricing-features style="border"]<br />';
			} else if(style == "style 2") {
			html += '[pricing-features style="none"]<br />';
			}
			html += '[pricing-list]3 Users[/pricing-list]<br />';
			html += '[pricing-list]2 Domains[/pricing-list]<br />';
			html += '[pricing-list]2 Databases[/pricing-list]<br />';

			html += '[/pricing-features]<br />';
			if(style == "style 1") {
			html += '[pricing-button parent="style 1" type="'+type+'" text="'+btntxt+'" url="'+url+'"]<br />';
			} else if(style == "style 2") {
			html += '[pricing-button parent="style 2" type="'+type+'" text="'+btntxt+'" url="'+url+'"]<br />';
			}

			
			html += '[/pricingtable]';
			
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
(function($){
	$('html').themeshortcode({
		id							: 'newsletter',
		title 						: 'Insert newsletter subscribe box',			
		image						: 'mail_check.png',
		showWindow					: true,		
		fields						: [ {type : 'text', name : 'Subscribe box title', id : 'title'} ,
										{type : 'text'  , name : 'Subscribe box text'    , id: 'text'},
										{type : 'text'  , name : 'Button text'    , id: 'btntxt'}] 
	}, 
	function(ed, url, options) 
	{	
		var title	= $('#'+options.pluginprefix + 'title').val();
		var text 	= $('#'+options.pluginprefix + 'text').val();
		var btntxt 	= $('#'+options.pluginprefix + 'btntxt').val();

		
		var html = '[newsletter title="'+ title +'" text="'+ text +'" buttontext="'+ btntxt +'"]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
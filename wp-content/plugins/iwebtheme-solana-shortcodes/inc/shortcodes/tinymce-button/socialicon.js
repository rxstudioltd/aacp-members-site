(function($){
	$('html').themeshortcode({
		id							: 'socialicon',
		title 						: 'Insert social icon',			
		image						: 'socialbutton.png',
		showWindow					: true,		
		windowWidth					: 800,
		fields						: [ {type : 'text', name : 'Icon link url ', id : 'url'} , 
										{type : 'socialicon'  , name : 'Choose social icon'    , id: 'socialicon'}] 
	}, 
	function(ed, url, options) 
	{	
		var socialicon 	= $('.social_icons .selected a').attr('class');	
		var url 	= $('#'+options.pluginprefix + 'url').val();
		
		var html = '[socialicon name="'+ socialicon +'" url="'+ url +'"]';
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
(function($){
	$('html').themeshortcode({
		id							: 'dropcaps',
		title 						: 'Drop cap',			
		image						: 'text_dropcaps.png',
		showWindow					: true,
		fields						: [ { type : 'select' , name : 'Drop cap type'	, id: 'select-type', option : ['dropcap1', 'dropcap2', 'dropcap3', 'dropcap4']},

										{ type : 'text' , name : 'Drop cap letter' , id: 'txt'},
										{ type : 'textarea' , name : 'Text block' , id: 'content'}
										] 
	}, 
	function(ed, url, options) 
	{
		var type 	= $('#'+options.pluginprefix + 'select-type').val();
		var txt 	= $('#'+options.pluginprefix + 'txt').val();
		var content 	= $('#'+options.pluginprefix + 'content').val();

		
		var html = '[dropcaps style="'+type+'" letter="'+txt+'" ]' + content + '[/dropcaps]';		
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
(function($){
	$('html').themeshortcode({
			id							: 'portfolioslider',
			title 						: 'Insert portfolio slideshow',			
			image						: 'testimonialslider.png',
			showWindow					: true,	
			windowWidth					: 800,
			fields						: [ {type : 'label',      name : 'Info'	, id: 'info'} ]
		}, 
		function(ed, url, options) 
		{
			var number 			= $('#'+options.pluginprefix + 'info').val();

			
			var html = '[testimonialslider]';			
			ed.execCommand('mceInsertContent', false, html);
		});
})(jQuery);
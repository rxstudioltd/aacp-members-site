(function($){
	$('html').themeshortcode({
		id							: 'infobox',
		title 						: 'Insert info box',			
		image						: 'infobox.png',
		showWindow					: true,		
		windowWidth					: 800,
		fields						: [ {type : 'select', name : 'Select info box style', id : 'select-style' , option : ['style 1','style 2','style 3','style 4','style 5','style 6','style 7','style 8']} ,		
										{type : 'text', name : 'Icon class name (font-awesome icons, see http://fortawesome.github.io/Font-Awesome/icons for icon class name)', id : 'icon'} ,
										{type : 'text', name : 'Info box title', id : 'boxtitle'} ,
										{type : 'text', name : 'Info box subtitle (not works for infobox style 1 and 2)', id : 'boxsubtitle'} ,
										{type : 'textarea', name : 'Info box content', id : 'boxcontent'} ,
										{type : 'inputgrow', name : 'Add list of text (not works for style 1 and 2)'},
										{type : 'text'  , name : 'Button link url'    , id: 'btnurl'},
										{type : 'text'  , name : 'Button Text'    , id: 'btntxt'} ] 
	}, 
	function(ed, url, options) 
	{	
		var style 	= $('#'+options.pluginprefix + 'select-style').val();
		var icon	= $('#'+options.pluginprefix + 'icon').val();
		var boxtitle 	= $('#'+options.pluginprefix + 'boxtitle').val();
		var boxsubtitle 	= $('#'+options.pluginprefix + 'boxsubtitle').val();
		var boxcontent 	= $('#'+options.pluginprefix + 'boxcontent').val();
		var btnurl 	= $('#'+options.pluginprefix + 'btnurl').val();
		var btntxt 	= $('#'+options.pluginprefix + 'btntxt').val();
		
		var boxstyle = '';
		if(style == "style 1") {
			boxstyle = 'info_box_style1 text_center';
			var html = '<div class="' + boxstyle + '">';
				html += '<h5><a href="' + btnurl +'">' + boxtitle +'</a></h5>[icon style="icon_style1 icon_rounded ' + icon +' icon-4x"]<div class="clearboth"></div>\n';
				html += '<p>' + boxcontent +'</p>\n';		
				html += '<a class="button secondary small right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';				
				html += '</div>';
		} else if(style == "style 2") {
			boxstyle = 'info_box_style2';
			var html = '<div class="' + boxstyle + '">';
				html += '<h5>[icon style="icon_style1 icon_rounded ' + icon +' icon-1x"]<a href="' + btnurl +'">' + boxtitle +'</a></h5>';
				html += '<p>' + boxcontent +'</p>\n';		
				html += '<a class="readmore_link align_right" href="' + btnurl +'">' + btntxt +'</a>';				
				html += '</div>';
		} else if(style == "style 3") {
			boxstyle = 'info_box_style3';
			var html = '<div class="' + boxstyle + '">';
				html += '<div class="clearboth"></div>\n[icon style="icon_style1 icon_rounded ' + icon +' icon-1x"]';
				html += '<div class="info_box_style3_content">';
				html += '<h4><a href="' + btnurl +'">' + boxtitle +'</a></h4>';
				html += '<h6 class="subheader">' + boxsubtitle +'</h6>';
				html += '<p>' + boxcontent +'</p>\n';	
				html += '<ul class="list check_list">';
					var i = 0;
					$(".textgrow").each(function(){
						if(i == 0) {
							html += '<li>' + $(this).val() + '</li>';
						} else {
							html += '<li>' + $(this).val() + '</li>';
						}
						i++;
					});
				html += '</ul>';				
				html += '<a class="button secondary small right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';	
				html += '</div>';				
				html += '</div>';
		} else if(style == "style 4") {
			boxstyle = 'info_box_style4';
			var html = '<div class="' + boxstyle + '">';
				html += '<div class="clearboth"></div>\n[icon style="icon_style1 icon_rounded ' + icon +' icon-2x"]';
				html += '<div class="info_box_style4_content">';
				html += '<h4><a href="' + btnurl +'">' + boxtitle +'</a></h4>';
				html += '<h6 class="subheader">' + boxsubtitle +'</h6>';
				html += '<p>' + boxcontent +'</p>\n';	
				html += '<ul class="list check_list">';
					var i = 0;
					$(".textgrow").each(function(){
						if(i == 0) {
							html += '<li>' + $(this).val() + '</li>';
						} else {
							html += '<li>' + $(this).val() + '</li>';
						}
						i++;
					});
				html += '</ul>';				
				html += '<a class="button secondary small right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';	
				html += '</div>';				
				html += '</div>';
		} else if(style == "style 5") {
			boxstyle = 'info_box_style5';
			var html = '<div class="' + boxstyle + '">';
				html += '<div class="clearboth"></div>\n[icon style="icon_style1 icon_rounded ' + icon +' icon-3x"]';
				html += '<div class="info_box_style5_content">';
				html += '<h4><a href="' + btnurl +'">' + boxtitle +'</a></h4>';
				html += '<h6 class="subheader">' + boxsubtitle +'</h6>';
				html += '<p>' + boxcontent +'</p>\n';	
				html += '<ul class="list check_list">';
					var i = 0;
					$(".textgrow").each(function(){
						if(i == 0) {
							html += '<li>' + $(this).val() + '</li>';
						} else {
							html += '<li>' + $(this).val() + '</li>';
						}
						i++;
					});
				html += '</ul>';				
				html += '<a class="button secondary small right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';	
				html += '</div>';				
				html += '</div>';
		} else if(style == "style 6") {
			boxstyle = 'info_box_style6';
			var html = '<div class="' + boxstyle + '">';
				html += '<div class="clearboth"></div>\n[icon style="icon_style1 icon_rounded ' + icon +' icon-4x"]';
				html += '<div class="info_box_style6_content">';
				html += '<h3><a href="' + btnurl +'">' + boxtitle +'</a></h3>';
				html += '<h5 class="subheader">' + boxsubtitle +'</h5>';
				html += '<p>' + boxcontent +'</p>\n';	
				html += '<ul class="list check_list">';
					var i = 0;
					$(".textgrow").each(function(){
						if(i == 0) {
							html += '<li>' + $(this).val() + '</li>';
						} else {
							html += '<li>' + $(this).val() + '</li>';
						}
						i++;
					});
				html += '</ul>';				
				html += '<a class="button secondary medium right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';	
				html += '</div>';				
				html += '</div>';
		} else if(style == "style 7") {
			boxstyle = 'info_box_style7';
			var html = '<div class="' + boxstyle + '">';
				html += '<div class="clearboth"></div>\n[icon style="icon_style1 icon_rounded ' + icon +' icon-5x"]';
				html += '<div class="info_box_style7_content">';
				html += '<h3><a href="' + btnurl +'">' + boxtitle +'</a></h3>';
				html += '<h5 class="subheader">' + boxsubtitle +'</h5>';
				html += '<p>' + boxcontent +'</p>\n';	
				html += '<ul class="list check_list">';
					var i = 0;
					$(".textgrow").each(function(){
						if(i == 0) {
							html += '<li>' + $(this).val() + '</li>';
						} else {
							html += '<li>' + $(this).val() + '</li>';
						}
						i++;
					});
				html += '</ul>';				
				html += '<a class="button secondary medium right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';	
				html += '</div>';				
				html += '</div>';
		} else if(style == "style 8") {
			boxstyle = 'info_box_style8';
			var html = '<div class="' + boxstyle + '">';
				html += '<div class="clearboth"></div>\n[icon style="icon_style1 icon_rounded ' + icon +' icon-5x"]';
				html += '<div class="info_box_style8_content">';
				html += '<h3><a href="' + btnurl +'">' + boxtitle +'</a></h3>';
				html += '<h5 class="subheader">' + boxsubtitle +'</h5>';
				html += '<p>' + boxcontent +'</p>\n';	
				html += '<ul class="list check_list">';
					var i = 0;
					$(".textgrow").each(function(){
						if(i == 0) {
							html += '<li>' + $(this).val() + '</li>';
						} else {
							html += '<li>' + $(this).val() + '</li>';
						}
						i++;
					});
				html += '</ul>';				
				html += '<a class="button secondary medium right_icon" href="' + btnurl +'"><i class="icon-share-alt align_right"></i>' + btntxt +'</a>';	
				html += '</div>';				
				html += '</div>';
		}
		
		
		ed.execCommand('mceInsertContent', false, html);
		ed.selection.collapse(0);
	});
})(jQuery);
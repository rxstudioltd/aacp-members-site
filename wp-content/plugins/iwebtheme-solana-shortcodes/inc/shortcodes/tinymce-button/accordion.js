(function($){
	$('html').themeshortcode({
		id							: 'accordion',
		title 						: 'Accordion',			
		image						: 'ui_accordion.png',
		showWindow					: true,	
		fields						: [ {type : 'inputgrow', name : 'Add Accordion'} ] 
	}, 
	function(ed, url, options) 
	{
		var html = '[accordion-wrapper]<br />';
		$(".textgrow").each(function(){
			html += '[accordion-title] ' + $(this).val() + ' [/accordion-title]<br />';
			html += '[accordion-body] Put your content here [/accordion-body]<br />';
		});
		html += '[/accordion-wrapper]<br />';

		
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
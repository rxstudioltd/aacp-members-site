(function($){
	$('html').themeshortcode({
		id							: 'featurelist',
		title 						: 'Insert feature list',			
		image						: 'features.png',
		showWindow					: true,	
		fields						: [ {type : 'label', name : 'Insert feature list'},{type : 'inputgrow', name : 'Add list'}] 
	}, 
	function(ed, url, options) 
	{
		var html = '[feature-lists]<br />';
			var i = 0;
			$(".textgrow").each(function(){
				if(i == 0) {
					html += '[feature-list]' + $(this).val() + '[/feature-list]<br />';
				} else {
					html += '[feature-list]' + $(this).val() + '[/feature-list]<br />';
				}
				i++;
			});
			html += '[/feature-lists]<br />';


			
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
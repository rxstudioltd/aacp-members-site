(function($){	
	
	/** themeshortcode **/
	var shortcode = $('html').themeshortcode({
			id							: 'blockquote',
			title 						: 'Blockquote',			
			image						: 'document-quote-icon.png',
			showWindow					: true,
			fields	: [ { type : 'select'  , name : 'Select blockquote type'     , id: 'select-type' , option : ['full', 'left aligned','right aligned']},
						{ type : 'textarea' 	, name : 'Content' 		, id : 'content'}]
		}, 
		function(ed, url, options) 
		{
			var btype	= $('#'+options.pluginprefix + 'select-type').val();
			$content	= $('#'+options.pluginprefix + 'content').val();
			
		var blocktype = '';
		if(btype == "full") {
			blocktype = 'pquote';
		} else if(btype == "left aligned") {
			blocktype = 'pquote_left';
		} else if(btype == "right aligned") {
			blocktype = 'pquote_right';
		}
		
			var insertHtml = '[blockquote type="' +blocktype+ '" content="' + $content + '"]';			
			ed.execCommand('mceInsertContent', false, insertHtml);
		});
	
})(jQuery);
(function($){
	$('html').themeshortcode({
		id							: 'alert',
		title 						: 'Alert box',			
		image						: 'Smiley.png',
		showWindow					: true,	
		fields						: [ {type : 'select', name : 'Select alert style '	, id : 'select-style' , option : ['notice', 'info', 'error', 'success']} ,
										{type : 'text'  , name : 'Alert Title'    	, id: 'title'},
		      						    {type : 'text'  , name : 'Alert Content'    	, id: 'content'}] 
	}, 
	function(ed, url, options) 
	{
		var style 	= $('#'+options.pluginprefix + 'select-style').val();
		var title	= $('#'+options.pluginprefix + 'title').val();
		var content	= $('#'+options.pluginprefix + 'content').val();

		
		var html = '[alert style="'+ style +'" title="'+ title +'" content="'+ content +'"]';
		
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
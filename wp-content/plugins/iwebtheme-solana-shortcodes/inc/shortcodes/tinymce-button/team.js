(function($){
	$('html').themeshortcode({
		id							: 'team',
		title 						: 'Insert team',			
		image						: 'team.png',
		showWindow					: true,	
		fields						: [ {type : 'text'  , name : 'Member name'     		, id: 'name'}, 
										{type : 'text'  , name : 'Role or position'     		, id: 'role'}, 
										{type : 'upload', name : 'Upload member image' 		, id: 'image'},
										{type : 'text'  , name : 'facebook profile url'     		, id: 'facebook'},
										{type : 'text'  , name : 'twitter username'     		, id: 'twitter'},
										{type : 'text'  , name : 'linkedin profile url'     		, id: 'linkedin'} ]
	}, 
	function(ed, url, options) 
	{
		var name		= $('#'+options.pluginprefix + 'name').val();
		var role		= $('#'+options.pluginprefix + 'role').val();
		var image		= $('#'+options.pluginprefix + 'image').val();
		var facebook		= $('#'+options.pluginprefix + 'facebook').val();
		var twitter		= $('#'+options.pluginprefix + 'twitter').val();
		var linkedin		= $('#'+options.pluginprefix + 'linkedin').val();
		var skype		= $('#'+options.pluginprefix + 'skype').val();

			
		var html = '[team name="'+ name +'" role="'+ role +'" img="'+ image +'" facebook="'+ facebook +'" twitter="'+ twitter +'" linkedin="'+ linkedin +'"]';
				
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
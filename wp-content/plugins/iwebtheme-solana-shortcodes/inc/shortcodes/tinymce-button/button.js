(function($){
	$('html').themeshortcode({
		id							: 'button',
		title 						: 'Insert button',			
		image						: 'button.png',
		showWindow					: true,	
		fields						: [ {type : 'select', name : 'Select button type ', id : 'select-type' , option : ['default','yellow','blue','aqua','gray','orange','green']} ,
										{type : 'select'  , name : 'Select button size'     , id: 'select-size' , option : ['small', 'medium', 'large']},
																				
										{type : 'text'  , name : 'Icon class name from font-awesome (leave blank for no icon button)'  , id: 'icon'},
										{type : 'text'  , name : 'Button link url'    , id: 'btnurl'},
		      						    {type : 'text'  , name : 'Button Text'    , id: 'btntxt'}] 
	}, 
	function(ed, url, options) 
	{
		var type 	= $('#'+options.pluginprefix + 'select-type').val();
		var size	= $('#'+options.pluginprefix + 'select-size').val();
		var iposition	= $('#'+options.pluginprefix + 'select-iposition').val();
		var icon 	= $('#'+options.pluginprefix + 'icon').val();	
		var txt 	= $('#'+options.pluginprefix + 'btntxt').val();		
		var url		= $('#'+options.pluginprefix + 'btnurl').val();

		if(icon !== "") {
			var html = '[button url="'+url+'" style="'+type+' '+size+'" icon="'+icon+'"]' + txt + '[/button]';
		} else if(icon == "") {
			var html = '[button url="'+url+'" style="'+type+' '+size+'"]' + txt + '[/button]';
		}
			
			
		ed.execCommand('mceInsertContent', false, html);
	});
})(jQuery);
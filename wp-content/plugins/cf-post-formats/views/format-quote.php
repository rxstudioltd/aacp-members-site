<div id="cfpf-format-quote-fields" style="display: none;">
	<div class="cf-elm-block">
		<label for="cfpf-format-quote-source-name"><?php _e('Quote content', 'cf-post-format'); ?></label>
		<textarea name="_format_quote_source_name" id="cfpf-format-quote-source-name" tabindex="1"><?php echo esc_attr(get_post_meta($post->ID, '_format_quote_source_name', true)); ?></textarea>
	</div>
</div>